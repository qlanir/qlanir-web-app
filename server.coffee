config = require "yaml-config"
express = require('express')
app = express()

$ENV = process.env.NODE_ENV || "dev"
opts = config.readConfig('./app.yml', $ENV)

app.get "/", (req, res) ->
  res.sendfile('html/index.html', {root : __dirname + "/public"})

app.get '/about', (req, res) ->
  res.sendfile('html/about.html', {root : __dirname + "/public"})

app
.use(express.static(__dirname + "/public"))
.use(express.favicon("public/img/favicon.ico"))
.listen(opts.server.port)


console.log "server [#{$ENV}] run on port #{opts.server.port}"