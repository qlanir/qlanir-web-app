describe "Subject items caching", ->

  itemsListService = null
  itemsService = null
  pinService = null
  cache = null
  $httpBackend = null
  baseUrl = null

  it "initialize", ->
    module("qlanir-app")
    inject (_itemsListService_, _cache_, _$httpBackend_, webApiConfig, _itemsService_, _pinService_) ->
      itemsListService = _itemsListService_
      itemsService = _itemsService_
      cache = _cache_
      $httpBackend = _$httpBackend_
      baseUrl = webApiConfig.url
      pinService = _pinService_

  describe "when two different items added, they both should be in cache", ->
    subj1 = id : 0, type : "person", label : "one", tags : [label : "zero"]
    subj2 = id : 1, type : "person", label : "two"

    it "add first item", ->
      $httpBackend.expectGET(baseUrl + 'people/0').respond(200, subj1)
      itemsListService.pushSubjectItem null, {id : 0, type : "person"}
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(1)
      expect(cache.findSubject id : 0, type : "person").toEqual(subj1)

    it "add second item", ->
      $httpBackend.expectGET(baseUrl + 'people/1').respond(200, subj2)
      itemsListService.pushSubjectItem null, {id : 1, type : "person"}
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(2)
      expect(cache.findSubject id : 1, type : "person").toEqual(subj2)

    it "get first item shouldn't request data from server", ->
      itemsListService.pushSubjectItem null, {id : 0, type : "person"}
      expect(cache._subjs.length).toEqual(2)
      expect(cache.findSubject id : 0, type : "person").toEqual(subj1)

    it "get second item shouldn't request data from server", ->
      itemsListService.pushSubjectItem null, {id : 1, type : "person"}
      expect(cache._subjs.length).toEqual(2)
      expect(cache.findSubject id : 1, type : "person").toEqual(subj2)

  describe "create subject and get it", ->
    subj = id : 2, type : "person", label : "three"

    it "create item", ->
      $httpBackend.expectPOST(baseUrl + 'people').respond(200, subj)
      itemsService.saveSubject label : "three", "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(3)
      expect(cache.findSubject id : 2, type : "person").toEqual(subj)

    it "get created item shouldn't request data from server", ->
      itemsListService.pushSubjectItem null, {id : 2, type : "person"}
      expect(cache._subjs.length).toEqual(3)
      expect(cache.findSubject id : 2, type : "person").toEqual(subj)

  describe "update subject and get it", ->
    subj = id : 2, type : "person", label : "three-updated"

    it "create item", ->
      $httpBackend.expectPUT(baseUrl + 'people/2').respond(200, subj)
      itemsService.saveSubject {id : 2, label : "three-updated"}, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(3)
      expect(cache.findSubject id : 2, type : "person").toEqual(subj)

    it "get created item shouldn't request data from server", ->
      itemsListService.pushSubjectItem null, {id : 2, type : "person"}
      expect(cache._subjs.length).toEqual(3)
      expect(cache.findSubject id : 2, type : "person").toEqual(subj)

  describe "remove subject and get it", ->
    subj = id : 2, type : "person", label : "three-updated"

    it "delete item", ->
      $httpBackend.expectDELETE(baseUrl + 'people/2').respond(200, ok : true)
      itemsListService.removeItem id : 2, type : "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(2)
      expect(cache.findSubject id : 2, type : "person").toBeFalsy()

  describe "work with linked subjects", ->

    hostSubjData = label : "host subject", people : [subject : { label : "linked subject" }]
    hostSubj = id : 3, type : "person", label : "host subject", people : [{id : 0, subject : {id : 4, label : "linked subject", type : "person"}}]
    linkedSubj = id : 4, type : "person", label : "linked subject", people : [{id : 1, subject : {id : 3, label : "host person", type : "person"}}]

    linkedSubjUpdData = id : 4, label : "linked subject", people : [{id : 1, subject : {id : 5, label : "host subject new"}}]
    linkedSubjUpd = id : 4, type : "person", label : "linked subject", people : [{id : 1, subject : {id : 5, label : "host subject new", type : "person"}}]
    hostSubjUpd = id : 3, type : "person", label : "host subject", people : []

    it "create host subject", ->
      $httpBackend.expectPOST(baseUrl + 'people').respond(200, hostSubj)
      itemsService.saveSubject hostSubjData, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(3)
      expect(cache.findSubject id : 3, type : "person").toEqual(hostSubj)

    it "load linked subject", ->
      $httpBackend.expectGET(baseUrl + 'people/4').respond(200, linkedSubj)
      itemsListService.pushSubjectItem null, {id : 4, type : "person"}
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache.findSubject id : 4, type : "person").toEqual(linkedSubj)

    it "update linked subject should reload related host subject in the cache", ->
      $httpBackend.expectPUT(baseUrl + 'people/4').respond(200, linkedSubjUpd)
      $httpBackend.expectGET(baseUrl + 'people/3').respond(200, hostSubjUpd)
      itemsService.saveSubject linkedSubjUpdData, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache.findSubject id : 4, type : "person").toEqual(linkedSubjUpd)
      expect(cache.findSubject id : 3, type : "person").toEqual(hostSubjUpd)

  describe "when subj in cache become linked to some subj", ->

    hostSubjData = id : 3, label : "host subject", people : [subject : { id : 1, label : "two" }]
    hostSubj = id : 3, label : "host subject", type : "person", people : [{id : 2, subject : { id : 1, label : "two" , type : "person"}}]
    secondSubjUpd = id : 1, label : "two", type : "person", people : [{id : 3, subject : { id : 3, label : "host subject", type : "person" }}]

    hostSubjRm = id : 3, label : "host subject", type : "person", people : []
    secondSubjRm = id : 1, label : "two", type : "person", people : []

    it "update host subject", ->
      $httpBackend.expectPUT(baseUrl + 'people/3').respond(200, hostSubj)
      $httpBackend.expectGET(baseUrl + 'people/1').respond(200, secondSubjUpd)
      itemsService.saveSubject hostSubjData, "person"

      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache.findSubject id : 1, type : "person").toEqual(secondSubjUpd)
      expect(cache.findSubject id : 3, type : "person").toEqual(hostSubj)

    it "unlink subjects", ->
      $httpBackend.expectDELETE(baseUrl + 'people/3/peopleLinks/2').respond(200, hostSubjRm)
      $httpBackend.expectGET(baseUrl + 'people/1').respond(200, secondSubjRm)
      itemsListService.removeLink hostSubj, hostSubj.people[0]
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache.findSubject id : 3, type : "person").toEqual(hostSubjRm)
      expect(cache.findSubject id : 1, type : "person").toEqual(secondSubjRm)

  describe "search results", ->

    searchReq = includeLinkedTags : true, type : "person", term : "host subject"
    searchRes = {"totalCount":{"people":1,"offers":0,"organizations":0},"term":"test","type":"person","results":[{"id":3,"name":"host subject","type":"person","hits":[],"tags":[]}]}
    searchResUpd = {"totalCount":{"people":1,"offers":0,"organizations":0},"term":"test","type":"person","results":[{"id":3,"name":"host subject updated","type":"person","hits":[],"tags":[]}]}
    subj1 = id : 0, type : "person", label : "one-updated"
    hostSubjUpd = id : 3, type : "person", label : "host subject upd"

    it "search host subject", ->
      $httpBackend.expectGET(baseUrl + 'search?includeLinkedTags=true&term=host+subject&type=person').respond(200, searchRes)
      itemsListService.pushSearchItem(searchReq)
      $httpBackend.flush()
      expect(cache._searchResults.length).toEqual(1)
      expect(cache.findSearchResults cache._searchResults[0].id).toEqual(searchRes)

    it "update first subject should refresh search item", ->
      $httpBackend.expectPUT(baseUrl + 'people/0').respond(200, subj1)
      $httpBackend.expectGET(baseUrl + 'search?includeLinkedTags=true&term=host+subject&type=person').respond(200, searchRes)
      itemsService.saveSubject {id : 0, label : "one-updated"}, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache.findSubject id : 0, type : "person").toEqual(subj1)

    it "update host subject should refresh search item", ->
      $httpBackend.expectPUT(baseUrl + 'people/3').respond(200, hostSubjUpd)
      $httpBackend.expectGET(baseUrl + 'search?includeLinkedTags=true&term=host+subject&type=person').respond(200, searchResUpd)
      itemsService.saveSubject {id : 3, label : "host subject upd"}, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(4)
      expect(cache._searchResults.length).toEqual(1)
      expect(cache.findSubject id : 3, type : "person").toEqual(hostSubjUpd)
      expect(cache.findSearchResults cache._searchResults[0].id).toEqual(searchResUpd)

    it "remove search result", ->
      itemsListService.removeItem id : -1, type : "search-results"
      expect(cache._searchResults.length).toEqual(0)

  describe "host subject contains 2 linked subjects", ->
    hostSubj2Data = label : "host subject 2", people : [{subject : {id : 6, label : "linked subject 1"}}, {subject : {id : 7, label : "linked subject 2"}}]
    hostSubj2 = id : 8, type : "person", label : "host subject 2", people : [{id : 6, subject : {id : 6, label : "linked subject 1", type : "person"}}, {id : 7, subject : {id : 7, label : "linked subject 2", type : "person"}}]
    linkedSubj1 = id : 6, type : "person", label : "linked subject 1", people : [{id : 7, subject : {id : 8, label : "host subject 2", type : "person"}}]
    linkedSubj2 = id : 7, type : "person", label : "linked subject 2", people : [{id : 8, subject : {id : 8, label : "host subject 2", type : "person"}}]

    hostSubjData2Upd = id : 8, label : "host subject 2 upd", people : [{subject : {id : 6, label : "linked subject 1"}}, {subject : {id : 7, label : "linked subject 2"}}]
    hostSubj2Upd = id : 8, type : "person", label : "host subject 2 upd", people : [{id : 6, subject : {id : 6, label : "linked subject 1", type : "person"}}, {id : 7, subject : {id : 7, label : "linked subject 2", type : "person"}}]
    linkedSubj1Upd = id : 6, type : "person", label : "linked subject 1", people : [{id : 7, subject : {id : 8, label : "host subject 2 upd", type : "person"}}]
    linkedSubj2Upd = id : 7, type : "person", label : "linked subject 2", people : [{id : 8, subject : {id : 8, label : "host subject 2 upd", type : "person"}}]

    it "create host subject", ->
      $httpBackend.expectPOST(baseUrl + 'people').respond(200, hostSubj2)
      itemsService.saveSubject hostSubj2Data, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(5)
      expect(cache.findSubject id : 8, type : "person").toEqual(hostSubj2)

    it "load linked subjects", ->
      $httpBackend.expectGET(baseUrl + 'people/6').respond(200, linkedSubj1)
      itemsListService.pushSubjectItem null, {id : 6, type : "person"}
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(6)
      expect(cache.findSubject id : 6, type : "person").toEqual(linkedSubj1)

      $httpBackend.expectGET(baseUrl + 'people/7').respond(200, linkedSubj2)
      itemsListService.pushSubjectItem null, {id : 7, type : "person"}
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(7)
      expect(cache.findSubject id : 7, type : "person").toEqual(linkedSubj2)

    it "update host subject should reload related linked subjects in the cache", ->
      $httpBackend.expectPUT(baseUrl + 'people/8').respond(200, hostSubj2Upd)
      $httpBackend.expectGET(baseUrl + 'people/6').respond(200, linkedSubj1Upd)
      $httpBackend.expectGET(baseUrl + 'people/7').respond(200, linkedSubj2Upd)
      itemsService.saveSubject hostSubjData2Upd, "person"
      $httpBackend.flush()
      expect(cache._subjs.length).toEqual(7)
      expect(cache.findSubject id : 8, type : "person").toEqual(hostSubj2Upd)
      expect(cache.findSubject id : 6, type : "person").toEqual(linkedSubj1Upd)
      expect(cache.findSubject id : 7, type : "person").toEqual(linkedSubj2Upd)

  describe "pins test", ->

    pinSubj = id : 9, label : "pin subject", type : "person"
    pinSubjUpd = id : 9, label : "pin subject upd", type : "person"
    pinItem = {"id":0,"subject":{"type":"person","id":9,"label":"pin subject"},"description":"test","userLinkType":"missing","userRole":null,"userProxy":null}
    pinItemUpd = {"id":0,"subject":{"type":"person","id":9,"label":"pin subject upd"},"description":"test","userLinkType":"missing","userRole":null,"userProxy":null}

    it "create subject and pin it", ->
      $httpBackend.expectPOST(baseUrl + 'people').respond(200, pinSubj)
      itemsService.saveSubject {label : "pin subject"}, "person"
      $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pinItem)
      itemsListService.pinItem {type : "search-results",  label : "test"}, {subject : id : 9}
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(1)
      expect(cache._pins[0]).toEqual(pinItem)

    it "update pin subj", ->
      $httpBackend.expectPUT(baseUrl + 'people/9').respond(200, pinSubjUpd)
      $httpBackend.expectGET(baseUrl + 'pins/0').respond(200, pinItemUpd)
      itemsService.saveSubject pinSubjUpd, "person"
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(1)
      expect(cache._pins[0]).toEqual(pinItemUpd)

    it "unpin subj", ->
      $httpBackend.expectDELETE(baseUrl + 'pins/0').respond(200, ok : true)
      pinService.unpin pinService.pins[0]
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(0)

  describe "pins with linked subjects test", ->
    pinSubjData = label : "pin subject x", people : [subject : {label : "linked subject x"}]
    pinSubj = id : 10, label : "pin subject x", type : "person", people : [{id : 11, subject : {id : 11, label : "linked subject x", type : "person"}}]
    pinSubjUpd = id : 10, label : "pin subject x", type : "person", people : [{id : 11, subject : {id : 11, label : "linked subject x upd", type : "person"}}]
    pinSubjRm = id : 10, label : "pin subject x", type : "person", people : []

    linkedPinSubjDataUpd = id : 11, label : "linked subject x upd", people : [{id : 12, subject : {id : 10, label : "pin subject x"}}]
    linkedPinSubj = id : 11, label : "linked subject x", type : "person", people : [{id : 12, subject : {id : 10, label : "pin subject x", type : "person"}}]
    linkedPinSubjUpd = id : 11, label : "linked subject x upd", type : "person", people : [{id : 12, subject : {id : 10, label : "pin subject x", type : "person"}}]
    linkedPinSubjRm = id : 11, label : "linked subject x upd", type : "person", people : []

    pinItem = {"id":1,"subject":{"type":"person","id":10,"label":"pin subject x"},"description":"test","userLinkType":"secondary","userRole":null, "userProxy" : { id : 11, label : "linked subject x", type : "person" }}
    pinItemUpd = {"id":1,"subject":{"type":"person","id":10,"label":"pin subject x"},"description":"test","userLinkType":"secondary","userRole":null,"userProxy" : { id : 11, label : "linked subject x upd", type : "person" }}
    pinItemRm = {"id":1,"subject":{"type":"person","id":10,"label":"pin subject x"},"description":"test","userLinkType":"missed","userRole":null,"userProxy" : null}

    it "create subjects and pin one", ->
      $httpBackend.expectPOST(baseUrl + 'people').respond(200, pinSubj)
      $httpBackend.expectGET(baseUrl + 'people/11').respond(200, linkedPinSubj)
      $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pinItem)
      itemsService.saveSubject pinSubjData, "person"
      itemsListService.pushSubjectItem null, {id : 11, type : "person"}
      itemsListService.pinItem {type : "search-results",  label : "test"}, {subject : id : 10}
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(1)
      expect(cache._pins[0]).toEqual(pinItem)

    xit "update linked subject should refresh pin", ->
      $httpBackend.expectPUT(baseUrl + 'people/11').respond(200, linkedPinSubjUpd)
      $httpBackend.expectGET(baseUrl + 'people/10').respond(200, pinSubjUpd)
      #$httpBackend.expectGET(baseUrl + 'pins/1').respond(200, pinItemUpd)
      itemsService.saveSubject linkedPinSubjDataUpd, "person"
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(1)
      expect(cache._pins[0]).toEqual(pinItemUpd)

    xit "remove links between subjects", ->
      console.log ">>>subjectsCacheSpec.coffee:295"
      $httpBackend.expectPUT(baseUrl + 'people/11').respond(200, linkedPinSubjRm)
      $httpBackend.expectGET(baseUrl + 'people/10').respond(200, pinSubjRm)
      #$httpBackend.expectGET(baseUrl + 'pins/1').respond(200, pinItemRm)
      itemsService.saveSubject linkedPinSubjRm, "person"
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(1)
      expect(cache._pins[0]).toEqual(pinItemUpd)

    it "unpin subj", ->
      $httpBackend.expectDELETE(baseUrl + 'pins/1').respond(200, ok : true)
      pinService.unpin pinService.pins[0]
      $httpBackend.flush()
      expect(cache._pins.length).toEqual(0)
