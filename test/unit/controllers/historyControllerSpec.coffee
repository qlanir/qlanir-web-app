"use strict"

xdescribe "History controller testing, starts with searching", ->

  scope = null
  meadowScope = null
  searchResult = null
  url = null

  beforeEach(module("qlanir-app"))

  beforeEach inject (webApiConfig) ->
    url = webApiConfig.url

  beforeEach inject ($httpBackend, $controller, $rootScope, historyService) ->
    scope = $rootScope.$new()
    $controller("historyController", $scope : scope, historyService : historyService)

  beforeEach inject ($httpBackend, $controller, $rootScope, searchService, search_result_fixture) ->
    searchScope = $rootScope.$new()
    $controller("rootController", $scope : searchScope)
    $controller("searchController", $scope : searchScope, searchService : searchService)
    $httpBackend.whenGET(url + 'search?').respond(search_result_fixture)
    searchScope.search().then (res) -> searchResult = res
    $httpBackend.flush()

  beforeEach inject ($httpBackend, $controller, $rootScope, itemsListService) ->
    meadowScope = $rootScope.$new()
    $controller("meadowController", $scope : meadowScope, itemsListService : itemsListService)

  openSubject = (issuer, id) ->
    inject ($httpBackend, subject_1_fixture, subject_2_fixture) ->
      $httpBackend.whenGET(url + 'search?').respond(subject_1_fixture)
      $httpBackend.whenGET(url + 'search?').respond(subject_2_fixture)
      meadowScope.openItem(issuer, id)
      $httpBackend.flush()

  iit "search result should be defined", ->
    expect(searchResult).toBeTruthy()
    expect(searchResult.term).toEqual("term")

  describe "open subj-1 form search", ->

    openSubj1 = ->
      openSubject(searchItem, 1)

    it "history should contain 2 items", ->
      openSubj1()
      expect(scope.stack).toBeTruthy()
      expect(scope.stack.length).toEqual(2)

    it "first item should be search item", ->
      openSubj1()
      expect(scope.stack[0].item.type).toEqual("search")

    it "2nd item should be person item", ->
      openSubj1()
      expect(scope.stack[1].item.type).toEqual("person")

    describe "restore search from history", ->

      restore = ->
        openSubj1()
        scope.restorePoint(scope.stack[0])

      it "meadow should contain 1 item", ->
        restore()
        expect(meadowScope.items.length).toEqual(1)

    describe "restore subj-1 from history", ->

      restore = ->
        openSubj1()
        scope.restorePoint(scope.stack[1])

      it "meadow should contain 2 item", ->
        restore()
        expect(meadowScope.items.length).toEqual(2)

    describe "open subj-2 from subj-1", ->

      openSubj2 = ->
        openSubj1()
        openSubject(scope.stack[1].item, 2)

      describe "restore search from history", ->

        restore = ->
          openSubj2()
          scope.restorePoint(scope.stack[0])

        it "meadow should contain 1 item", ->
          restore()
          expect(meadowScope.items.length).toEqual(1)

      describe "restore subj-1 from history", ->

        restore = ->
          openSubj2()
          scope.restorePoint(scope.stack[1])

        it "meadow should contain 2 item", ->
          restore()
          expect(meadowScope.items.length).toEqual(2)

      describe "restore subj-2 from history", ->

        restore = ->
          openSubj2()
          scope.restorePoint(scope.stack[2])

        it "meadow should contain 3 item", ->
          restore()
          expect(meadowScope.items.length).toEqual(3)

      it "history should contain 3 items", ->
        openSubj2()
        expect(scope.stack).toBeTruthy()
        expect(scope.stack.length).toEqual(3)

      it "first item should be search item", ->
        openSubj2()
        expect(scope.stack[0].item.type).toEqual("search")

      it "2nd and 3rd items should be person item", ->
        openSubj2()
        expect(scope.stack[1].item.type).toEqual("person")
        expect(scope.stack[2].item.type).toEqual("person")

      describe "open subj-2 from subj-1", ->

        openSubj2 = ->
          openSubj1()
          openSubject(scope.stack[1].item, 2)

        it "history should contain 3 items (nothig should be changed)", ->
          openSubj2()
          expect(scope.stack).toBeTruthy()
          expect(scope.stack.length).toEqual(3)

        it "history first item should be search item", ->
          openSubj2()
          expect(scope.stack[0].item.type).toEqual("search")

        it "history 2nd and 3rd items should be person item", ->
          openSubj2()
          expect(scope.stack[1].item.type).toEqual("person")
          expect(scope.stack[2].item.type).toEqual("person")

      describe "open subj-1 from search", ->

        openSubj1FromSearch = ->
          openSubj2()
          openSubj1()

        it "history should contain 4 items", ->
          openSubj1FromSearch()
          expect(scope.stack).toBeTruthy()
          expect(scope.stack.length).toEqual(4)

        it "history first item should be search item", ->
          openSubj1FromSearch()
          expect(scope.stack[0].item.type).toEqual("search")

        it "history 2nd item should be person item", ->
          openSubj1FromSearch()
          expect(scope.stack[1].item.type).toEqual("person")

        it "history 2nd item should be subj-1", ->
          openSubj1FromSearch()
          expect(scope.stack[1].item.id).toEqual(1)


        describe "restore search from history", ->

          restore = ->
            openSubj2()
            scope.restorePoint(scope.stack[0])

          it "meadow should contain 1 item", ->
            restore()
            expect(meadowScope.items.length).toEqual(1)

        describe "restore subj-1 from history", ->

          restore = ->
            openSubj1FromSearch()
            scope.restorePoint(scope.stack[1])

          it "meadow should contain 2 item", ->
            restore()
            expect(meadowScope.items.length).toEqual(2)

        describe "restore subj-2 from history", ->

          restore = ->
            openSubj1FromSearch()
            scope.restorePoint(scope.stack[2])

          it "meadow should contain 3 item", ->
            restore()
            expect(meadowScope.items.length).toEqual(3)


        describe "restore subj-1 (from earch) from history", ->

          restore = ->
            openSubj1FromSearch()
            scope.restorePoint(scope.stack[3])

          it "meadow should contain 2 item", ->
            restore()
            expect(meadowScope.items.length).toEqual(2)
