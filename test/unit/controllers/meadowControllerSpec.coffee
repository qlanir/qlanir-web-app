"use strict"

xdescribe "Meadow controller testing, starts with searching", ->

  scope = null
  searchItem = null

  beforeEach(module("qlanir-app"))

  beforeEach inject ($httpBackend, $controller, $rootScope, searchService, search_result_fixture) ->
    searchScope = $rootScope.$new()
    $controller("searchController", $scope : searchScope, searchService : searchService)
    $httpBackend.whenGET('//localhost:8001/data/search/1').respond(search_result_fixture)
    searchScope.search().then (res) -> searchItem = res
    $httpBackend.flush()

  openSubject = (issuer, id) ->
    inject ($httpBackend, $controller, $rootScope, itemsListService, subject_1_fixture, subject_2_fixture) ->
      scope = $rootScope.$new()
      $controller("meadowController", $scope : scope, itemsListService : itemsListService)
      $httpBackend.whenGET('//localhost:8001/data/subjects/1').respond(subject_1_fixture)
      $httpBackend.whenGET('//localhost:8001/data/subjects/2').respond(subject_2_fixture)
      scope.openItem(issuer, id)
      $httpBackend.flush()

  it "search item should be defined", ->
    expect(searchItem).toBeTruthy()
    expect(searchItem.type).toEqual("search")

  describe "open subj-1 form search", ->

    openSubj1 = ->
      openSubject(searchItem, 1)

    it "meadow should contain 2 items", ->
      openSubj1()
      expect(scope.items).toBeTruthy()
      expect(scope.items.length).toEqual(2)

    it "first item should be search item", ->
      openSubj1()
      expect(scope.items[0].type).toEqual("search")

    it "2nd item should be person item", ->
      openSubj1()
      expect(scope.items[1].type).toEqual("person")

    describe "open subj-2 from subj-1", ->

      openSubj2 = ->
        openSubj1()
        openSubject(scope.items[1], 2)

      it "meadow should contain 3 items", ->
        openSubj2()
        expect(scope.items).toBeTruthy()
        expect(scope.items.length).toEqual(3)

      it "first item should be search item", ->
        openSubj2()
        expect(scope.items[0].type).toEqual("search")

      it "2nd and 3rd items should be person item", ->
        openSubj2()
        expect(scope.items[1].type).toEqual("person")
        expect(scope.items[2].type).toEqual("person")

      describe "open subj-2 from subj-1", ->

        openSubj2 = ->
          openSubj1()
          openSubject(scope.items[1], 2)

        it "meadow should contain 3 items (nothig should be changed)", ->
          openSubj2()
          expect(scope.items).toBeTruthy()
          expect(scope.items.length).toEqual(3)

        it "first item should be search item", ->
          openSubj2()
          expect(scope.items[0].type).toEqual("search")

        it "2nd and 3rd items should be person item", ->
          openSubj2()
          expect(scope.items[1].type).toEqual("person")
          expect(scope.items[2].type).toEqual("person")

      describe "open subj-1 from search", ->

        openSubj1FromSearch = ->
          openSubj2()
          openSubj1()

        it "meadow should contain 2 items", ->
          openSubj1FromSearch()
          expect(scope.items).toBeTruthy()
          expect(scope.items.length).toEqual(2)

        it "first item should be search item", ->
          openSubj1FromSearch()
          expect(scope.items[0].type).toEqual("search")

        it "2nd item should be person item", ->
          openSubj1FromSearch()
          expect(scope.items[1].type).toEqual("person")

        it "2nd item should be subj-1", ->
          openSubj1FromSearch()
          expect(scope.items[1].id).toEqual(1)
