"use strict"

xdescribe "Search controller testing", ->

  scope = null

  beforeEach(module("qlanir-app"))

  beforeEach inject ($httpBackend, $controller, $rootScope, searchService, search_result_fixture) ->
    scope = $rootScope.$new()
    $controller("searchController", $scope : scope, searchService : searchService)
    $httpBackend.whenGET('//localhost:8001/data/search/1').respond(search_result_fixture)
    scope.search()
    $httpBackend.flush()

  describe "search", ->

    it "should get result", ->
      expect(scope.searchResult).toBeTruthy()
      expect(scope.searchResult.peopleCount).toEqual(3)
      expect(scope.searchResult.offersCount).toEqual(0)
      expect(scope.searchResult.organizationsCount).toEqual(1)

