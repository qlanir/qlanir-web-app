"use strict"

xdescribe "Pin controller testing, starts with searching", ->

  scope = null
  meadowScope = null
  searchItem = null

  beforeEach(module("qlanir-app"))

  beforeEach inject ($httpBackend, $controller, $rootScope, pinService) ->
    scope = $rootScope.$new()
    $controller("pinController", $scope : scope, pinService : pinService)

  beforeEach inject ($httpBackend, $controller, $rootScope, searchService, search_result_fixture) ->
    searchScope = $rootScope.$new()
    $controller("searchController", $scope : searchScope, searchService : searchService)
    $httpBackend.whenGET('//localhost:8001/data/search/1').respond(search_result_fixture)
    searchScope.search().then (res) -> searchItem = res
    $httpBackend.flush()

  beforeEach inject ($httpBackend, $controller, $rootScope, itemsListService) ->
    meadowScope = $rootScope.$new()
    $controller("meadowController", $scope : meadowScope, itemsListService : itemsListService)

  openSubject = (issuer, id) ->
    inject ($httpBackend, subject_1_fixture, subject_2_fixture) ->
      $httpBackend.whenGET('//localhost:8001/data/subjects/1').respond(subject_1_fixture)
      $httpBackend.whenGET('//localhost:8001/data/subjects/2').respond(subject_2_fixture)
      meadowScope.openItem(issuer, id)
      $httpBackend.flush()

  it "search item should be defined", ->
    expect(searchItem).toBeTruthy()
    expect(searchItem.type).toEqual("search")

  describe "open subj-1 form search", ->

    openSubj1 = ->
      openSubject(searchItem, 1)

    describe "pin subj-1", ->

      pinSubj1 =->
        openSubj1()
        meadowScope.pinItem meadowScope.items[0]

      it "pins should have one item", ->
        pinSubj1()
        expect(scope.pins.length).toEqual(1)

      describe "pin subj-1", ->

        it "pins should have one item", ->
          pinSubj1()
          expect(scope.pins.length).toEqual(1)

      describe "pin subj-2", ->

        pinSubj2 =->
          pinSubj1()
          meadowScope.pinItem meadowScope.items[1]

        it "pins should have 2 items", ->
          pinSubj2()
          expect(scope.pins.length).toEqual(2)

      describe "unpin subj-1", ->

        unpinSubj1 =->
          scope.unpin scope.pins[0]

        it "pins should have zero items", ->
          pinSubj1()
          unpinSubj1()
          expect(scope.pins.length).toEqual(0)


