"use strict"

"""
1.при удалении субъекта к которому привязан пин(ы), пин(ы) удаляется с ошибкой, после этой ошибки клиент начинает некорректно работать.
2.при изменении субъекта, обновляется только первый попавшийся привязанный пин, остальные не обновляются (возможно так же и с удалением? визуально не проверить из-за предыдущей ошибки)
"""

describe "pins-qp123", ->

  itemsListService = null
  itemsService = null
  pinService = null
  cache = null
  $httpBackend = null
  $rootScope = null
  baseUrl = null

  beforeEach ->
    module("qlanir-app")

  beforeEach inject (_itemsListService_, _cache_, _$httpBackend_, webApiConfig, _itemsService_, _pinService_, _$rootScope_) ->
      itemsListService = _itemsListService_
      itemsService = _itemsService_
      cache = _cache_
      $httpBackend = _$httpBackend_
      baseUrl = webApiConfig.url
      pinService = _pinService_
      $rootScope = _$rootScope_

  it "при удалении субъекта к которому привязан пин(ы), пин(ы) удаляется с ошибкой, после этой ошибки клиент начинает некорректно работать.", ->

    #create subject A
    subj = id : 1, type : "person", label : "subj"
    $httpBackend.expectPOST(baseUrl + 'people').respond(200, subj)
    itemsService.saveSubject(label : "subj", "person")
    $httpBackend.flush()

    #push item to desktop
    itemsListService.pushSubjectItem(null, subj)
    $rootScope.$apply()
    expect(itemsListService.items.length).toEqual(1)

    #create pin A1 linked to subject A
    pin1 = {"id":1,"subject":{"type":"person","id":1,"label":"subj"},"description":"one","userLinkType":"none","userRole":null, "userProxy" : null}
    $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pin1)
    itemsListService.pinItem({type : "search-results",  label : "one"}, {subject : id : 1, type : "person"})
    $httpBackend.flush()

    #create pin A2  linked to subject A
    pin2 = {"id":2,"subject":{"type":"person","id":1,"label":"subj"},"description":"two","userLinkType":"none","userRole":null, "userProxy" : null}
    $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pin2)
    itemsListService.pinItem({type : "search-results",  label : "two"}, {subject :  id : 1, type : "person"})
    $httpBackend.flush()

    expect(cache._pins.length).toEqual(2)
    expect(pinService.pins.length).toEqual(2)

    #remove subject A
    $httpBackend.expectDELETE(baseUrl + 'people/1').respond(200, ok : true)
    itemsListService.removeItem(id : 1, type : "person")
    $httpBackend.flush()

    expect(cache._pins.length).toEqual(0)
    expect(pinService.pins.length).toEqual(0)
    expect(itemsListService.items.length).toEqual(1)

  it "при изменении субъекта, обновляется только первый попавшийся привязанный пин, остальные не обновляются", ->

    #create subject A
    subj = id : 1, type : "person", label : "subj"
    $httpBackend.expectPOST(baseUrl + 'people').respond(200, subj)
    itemsService.saveSubject(label : "subj", "person")
    $httpBackend.flush()

    #push item to desktop
    itemsListService.pushSubjectItem(null, subj)
    $rootScope.$apply()
    expect(itemsListService.items.length).toEqual(1)

    #create pin A1 linked to subject A
    pin1 = {"id":1,"subject":{"type":"person","id":1,"label":"subj"},"description":"one","userLinkType":"none","userRole":null, "userProxy" : null}
    $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pin1)
    itemsListService.pinItem({type : "search-results",  label : "one"}, {subject : id : 1, type : "person"})
    $httpBackend.flush()

    #create pin A2 linked to subject A
    pin2 = {"id":2,"subject":{"type":"person","id":1,"label":"subj"},"description":"two","userLinkType":"none","userRole":null, "userProxy" : null}
    $httpBackend.expectPOST(baseUrl + 'pins').respond(200, pin2)
    itemsListService.pinItem({type : "search-results",  label : "two"}, {subject :  id : 1, type : "person"})
    $httpBackend.flush()

    expect(cache._pins.length).toEqual(2)
    expect(pinService.pins.length).toEqual(2)

    #update subject A
    updSubj = id : 1, type : "person", label : "updsubj"
    updPin1 = {"id":1,"subject":{"type":"person","id":1,"label":"updsubj"},"description":"one","userLinkType":"none","userRole":null, "userProxy" : null}
    updPin2 = {"id":2,"subject":{"type":"person","id":1,"label":"updsubj"},"description":"one","userLinkType":"none","userRole":null, "userProxy" : null}
    $httpBackend.expectPUT(baseUrl + 'people/1').respond(200, updSubj)
    $httpBackend.expectGET(baseUrl + 'pins/1').respond(200, updPin1)
    $httpBackend.expectGET(baseUrl + 'pins/2').respond(200, updPin2)
    itemsService.saveSubject(updSubj, "person")
    $httpBackend.flush()

    expect(cache._pins.length).toEqual(2)
    expect(pinService.pins.length).toEqual(2)
    expect(itemsListService.items.length).toEqual(1)

    expect(pinService.pins[0].item).toEqual(updPin1)
    expect(pinService.pins[1].item).toEqual(updPin2)

