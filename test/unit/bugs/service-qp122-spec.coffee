"use strict"


describe "service-qp122", ->

  itemsListService = null
  itemsService = null
  pinService = null
  cache = null
  $httpBackend = null
  $rootScope = null
  baseUrl = null

  beforeEach ->
    module("qlanir-app")

  beforeEach inject (_itemsListService_, _cache_, _$httpBackend_, webApiConfig, _itemsService_, _pinService_, _$rootScope_) ->
    itemsListService = _itemsListService_
    itemsService = _itemsService_
    cache = _cache_
    $httpBackend = _$httpBackend_
    baseUrl = webApiConfig.url
    pinService = _pinService_
    $rootScope = _$rootScope_

  it "при удалении линка с offer из субъекта который содержит offer, не удаляется из кеша привязанный offer, а значит не обновляется и сопутствующий service", ->

    # Display service with offer
    serviceResponse = {
      "type":"service",
      "offers":[
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":28226,"label":null,"owner":13394},
          "id":28226,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},"hostTag":null
        }
      ],
      "id":9060,
      "label":"test",
      "owner":13394
    }

    $httpBackend.expectGET(baseUrl + 'services/9060').respond(200, serviceResponse)
    itemsListService.pushSubjectItem(null, id : 9060, type : "service")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(1)

    # Display offer
    offerResponse = {
      "tags":[{"type":"tag","id":9060,"label":"test","owner":13394}],
      "contacts":[],
      "offers":[],
      "people":[
        {
          "proxy":null,
          "subject": {
            "birthdate" : null,
            "nameParts" : {
              "firstName":"person23",
              "lastName":null,
              "middleName":null,
              "nickName":null
            },
            "sex":null,
            "type":"person",
            "id":31375,
            "label":"person23",
            "owner":13394},
          "id":27412,
          "tag":{"type":"tag","id":-42,"label":null,"owner":13394},
          "hostTag":{"type":"tag","id":-41,"label":null,"owner":13394}}],
      "organizations":[],
      "type":"offer",
      "id":28226,
      "label":null,
      "owner":13394
    }

    $httpBackend.expectGET(baseUrl + 'offers/28226').respond(200, offerResponse)
    itemsListService.pushSubjectItem(itemsListService.items[0], id : 28226, type : "offer")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(2)

    # Display contact
    contactResponse =
    {
      "tags": [],
      "contacts": [],
      "offers": [
        {
          "proxy": null,
          "subject": {
            "type": "offer",
            "id": 28226,
            "label": null,
            "owner": 13394
          },
          "id": 9060,
          "tag": {
            "type": "tag",
            "id": 9060,
            "label": "test",
            "owner": 13394
          },
          "hostTag": null
        }
      ],
      "people": [],
      "organizations": [],
      "birthdate": null,
      "nameParts": {
        "firstName": "person23",
        "lastName": null,
        "middleName": "",
        "nickName": null
      },
      "sex": null,
      "type": "person",
      "id": 31375,
      "label": "person23",
      "owner": 13394
    }

    $httpBackend.expectGET(baseUrl + 'people/31375').respond(200, contactResponse)
    itemsListService.pushSubjectItem(itemsListService.items[1], id : 31375, type : "person")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(3)


    #Remove link between contact and offer
    updContactResponse =
    {
      "tags": [],
      "contacts": [],
      "offers": [],
      "people": [],
      "organizations": [],
      "birthdate": null,
      "nameParts": {
        "firstName": "person23",
        "lastName": null,
        "middleName": "",
        "nickName": null
      },
      "sex": null,
      "type": "person",
      "id": 31375,
      "label": "person23",
      "owner": 13394
    }

    $httpBackend.expectDELETE(baseUrl + 'offers/28226').respond(200, ok : true)
    $httpBackend.expectGET(baseUrl + 'services/9060').respond(404)
    $httpBackend.expectGET(baseUrl + 'people/31375').respond(200, updContactResponse)

    itemsListService.removeLink(itemsListService.items[2].subject, itemsListService.items[2].subject.offers[0])
    $httpBackend.flush()
    expect(itemsListService.items.length).toEqual(3)
    expect(cache._subjs.length).toEqual(1)

    expect(itemsListService.items[0]._isRemoved).toEqual(true)
    expect(itemsListService.items[1]._isRemoved).toEqual(true)
    expect(itemsListService.items[2]._isRemoved).toEqual(undefined)
    expect(itemsListService.items[2].subject).toEqual(updContactResponse)

  it "при удалении самого субъекта который содержит offer, не удаляется из кеша привязанный offer , а значит не обновляется и сопутствующий service", ->

    # Display service with offer
    serviceResponse = {
      "type":"service",
      "offers":[
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":28226,"label":null,"owner":13394},
          "id":28226,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},"hostTag":null
        }
      ],
      "id":9060,
      "label":"test",
      "owner":13394
    }

    $httpBackend.expectGET(baseUrl + 'services/9060').respond(200, serviceResponse)
    itemsListService.pushSubjectItem(null, id : 9060, type : "service")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(1)

    # Display offer
    offerResponse = {
      "tags":[{"type":"tag","id":9060,"label":"test","owner":13394}],
      "contacts":[],
      "offers":[],
      "people":[
        {
          "proxy":null,
          "subject": {
            "birthdate" : null,
            "nameParts" : {
              "firstName":"person23",
              "lastName":null,
              "middleName":null,
              "nickName":null
            },
            "sex":null,
            "type":"person",
            "id":31375,
            "label":"person23",
            "owner":13394},
          "id":27412,
          "tag":{"type":"tag","id":-42,"label":null,"owner":13394},
          "hostTag":{"type":"tag","id":-41,"label":null,"owner":13394}}],
      "organizations":[],
      "type":"offer",
      "id":28226,
      "label":null,
      "owner":13394
    }

    $httpBackend.expectGET(baseUrl + 'offers/28226').respond(200, offerResponse)
    itemsListService.pushSubjectItem(itemsListService.items[0], id : 28226, type : "offer")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(2)

    # Display contact
    contactResponse =
    {
      "tags": [],
      "contacts": [],
      "offers": [
        {
          "proxy": null,
          "subject": {
            "type": "offer",
            "id": 28226,
            "label": null,
            "owner": 13394
          },
          "id": 9060,
          "tag": {
            "type": "tag",
            "id": 9060,
            "label": "test",
            "owner": 13394
          },
          "hostTag": null
        }
      ],
      "people": [],
      "organizations": [],
      "birthdate": null,
      "nameParts": {
        "firstName": "person23",
        "lastName": null,
        "middleName": "",
        "nickName": null
      },
      "sex": null,
      "type": "person",
      "id": 31375,
      "label": "person23",
      "owner": 13394
    }

    $httpBackend.expectGET(baseUrl + 'people/31375').respond(200, contactResponse)
    itemsListService.pushSubjectItem(itemsListService.items[1], id : 31375, type : "person")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(3)

    #Remove contact
    $httpBackend.expectDELETE(baseUrl + 'people/31375').respond(200, ok : true)
    $httpBackend.expectGET(baseUrl + 'services/9060').respond(404)
    $httpBackend.expectGET(baseUrl + 'offers/28226').respond(404)

    itemsListService.removeItem(itemsListService.items[2])
    $httpBackend.flush()
    expect(itemsListService.items.length).toEqual(3)
    expect(cache._subjs.length).toEqual(0)
    expect(itemsListService.items[0]._isRemoved).toEqual(true)
    expect(itemsListService.items[1]._isRemoved).toEqual(true)
    expect(itemsListService.items[2]._isRemoved).toEqual(true)
