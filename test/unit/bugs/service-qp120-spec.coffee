"use strict"


describe "service-qp120", ->

  itemsListService = null
  itemsService = null
  pinService = null
  cache = null
  $httpBackend = null
  $rootScope = null
  baseUrl = null

  beforeEach ->
    module("qlanir-app")

  beforeEach inject (_itemsListService_, _cache_, _$httpBackend_, webApiConfig, _itemsService_, _pinService_, _$rootScope_) ->
    itemsListService = _itemsListService_
    itemsService = _itemsService_
    cache = _cache_
    $httpBackend = _$httpBackend_
    baseUrl = webApiConfig.url
    pinService = _pinService_
    $rootScope = _$rootScope_

  it "При добавлении offer не обновляется соответствующий service, который в кеше и отображен на экране.", ->

    # Get service with to offers
    serviceResponse = {
      "type":"service",
      "offers":[
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":27226,"label":"xxxx","owner":13394},
          "id":27226,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},"hostTag":null},
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,"subject":{"type":"offer","id":28226,"label":null,"owner":13394},
          "id":28226,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},"hostTag":null}
      ],
      "id":9060,
      "label":"test","owner":13394
    }

    $httpBackend.expectGET(baseUrl + 'services/9060').respond(200, serviceResponse)
    itemsListService.pushSubjectItem(null, id : 9060, type : "service")
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(1)

    # Create new offer with the same type
    newOffer = {"id":null,"description":null,"type":{"id":9060,"label":"test"},"linkedSubject":{"id":null,"label":"serv-contact","type":"person"}}

    newOfferResponse = {
      "tags":[{"type":"tag","id":9060,"label":"test","owner":13394}],
      "contacts":[],
      "offers":[],
      "people":[
        {
          "proxy":null,
          "subject": {
            "birthdate" : null,
            "nameParts" : {
              "firstName":"serv-contact",
              "lastName":null,
              "middleName":null,"nickName":null
            },
            "sex":null,
            "type":"person",
            "id":28133,
            "label":"serv-contact",
            "owner":13394},
          "id":27412,
          "tag":{"type":"tag","id":-42,"label":null,"owner":13394},
          "hostTag":{"type":"tag","id":-41,"label":null,"owner":13394}}],
      "organizations":[],
      "type":"offer","id":26315,"label":null,"owner":13394}

    updServiceResponse = {
      "type":"service",
      "offers":[
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":27226,"label":"xxxx","owner":13394},
          "id":27226,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},
          "hostTag":null},
        {
          "hostSubject":{"type":"person","id":31375,"label":"person23","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":28227,"label":null,"owner":13394},
          "id":28227,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},
          "hostTag":null},
        {
          "hostSubject":{"type":"person","id":27412,"label":"serv-contact","owner":13394},
          "proxy":null,
          "subject":{"type":"offer","id":28228,"label":null,"owner":13394},
          "id":26315,
          "tag":{"type":"tag","id":9060,"label":"test","owner":13394},
          "hostTag":null}
      ],
      "id":9060,
      "label":"test",
      "owner":13394
    }

    console.log ">>>service-qp120-spec.coffee:112"
    $httpBackend.expectPOST(baseUrl + 'offers').respond(200, newOfferResponse)
    $httpBackend.expectGET(baseUrl + 'services/9060').respond(200, updServiceResponse)
    itemsService.saveSubject newOffer, "offer"
    $httpBackend.flush()

    expect(itemsListService.items.length).toEqual(1)
    expect(cache._subjs.length).toEqual(2)
    expect(cache._subjs[0]).toEqual(updServiceResponse)
    expect(cache._subjs[1]).toEqual(newOfferResponse)
    expect(itemsListService.items[0].subject).toEqual(updServiceResponse)




