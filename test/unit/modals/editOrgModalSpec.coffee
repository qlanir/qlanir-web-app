#modal test examples `https://github.com/angular-ui/bootstrap/blob/master/src/modal/test/modal.spec.js`

describe "When fields in `Create new org modal` is set, we should send to offer correct data", ->

  baseUrl = null
  orgModal = null
  $rootScope = null
  $httpBackend = null

  open = (modalOptions) ->
    modal = orgModal.open(modalOptions);
    $rootScope.$digest()
    modal

  beforeEach(module("qlanir-app"))

  beforeEach inject (_orgModal_, _$rootScope_, _$httpBackend_, webApiConfig) ->
    $rootScope = _$rootScope_
    $httpBackend = _$httpBackend_
    orgModal = _orgModal_
    baseUrl = webApiConfig.url


  it "when scope's field `orgName` is set", ->

    expected = "label":"org","contacts":[],"people":[],"organizations":[],"offers":[],"tags":[]

    modalInstance = open template : "<form name='data.orgModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.orgName = 'org'

    $httpBackend.expectPOST(baseUrl + 'organizations', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

  it "when all fields are set", ->

    data =
      label : "org"
      contacts : [
        {subject : {contactType : -15, label : "skype contact"}},
        { id : 38, subject : {contactType : -25, label : "email@mail.ru"}}
      ]
      people : [tag : {id : -15}, subject : label : "org's person"]
      organizations : []
      offers : []
      tags : [label : "some new tag"]

    expectedResponse =
      id : 1
      label : "org"
      type : "organization"
      contacts : [
        {id : 1, subject : {contactType : -15, label : "skype contact", type : "contact"}},
        {id : 2, subject : {contactType : -25, label : "email@mail.ru", type : "contact"}}
      ]
      people : [id : 3, tag : {id : -15}, subject : id : 2, type : "person", label : "org's person"]
      organizations : []
      offers : []
      tags : [id : 1, label : "some new tag"]

    open template : "<form name='data.orgModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.orgName = 'org'
    scope.data.contacts = [
      {contactType : -15, contactValue : "skype contact"},
      { id : 38, contactType : -25, contactValue : "email@mail.ru"}
    ]
    scope.data.people = [personName : "org's person", personRole : id : -15]
    scope.data.tags = [label : "some new tag"]

    $httpBackend.expectPOST(baseUrl + 'organizations', data).respond(200, expectedResponse)
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

