#modal test examples `https://github.com/angular-ui/bootstrap/blob/master/src/modal/test/modal.spec.js`

xdescribe "When fields in `Create new person modal` is set, we should send to offer correct data", ->

  baseUrl = null
  personModal = null
  $rootScope = null
  $httpBackend = null

  open = (modalOptions) ->
    modal = personModal.open(modalOptions);
    $rootScope.$digest()
    modal

  beforeEach(module("qlanir-app"))

  beforeEach inject (_personModal_, _$rootScope_, _$httpBackend_, webApiConfig) ->
    $rootScope = _$rootScope_
    $httpBackend = _$httpBackend_
    personModal = _personModal_
    baseUrl = webApiConfig.url

  it "when scope's field `personName` is set", ->

    expected = "label":"max","nameParts":{"firstName":"max"},"birthDate":null,sex:null,"contacts":[],"people":[],"organizations":[],"offers":[],"tags":[]

    modalInstance = open template : "<form name='data.personModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.name =
      fullName : 'max'
      firstName : 'max'

    $httpBackend.expectPOST(baseUrl + 'people', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

  it "when all fields except linked people are set", ->

    expected =
      label : "max"
      nameParts :
        firstName : 'max'
      birthDate : "10.10.1980"
      sex : null
      contacts : [ subject : {contactType : -15, label : "skype contact"} ]
      people : [ ]
      organizations : [subject : label : "person's org"]
      offers : [tag : {label : "can do something"}, "subject":{}, "proxy":{}]
      tags : [label : "some new tag"]

    modalInstance = open template : "<form name='data.personModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.name =
      fullName : 'max'
      firstName : 'max'
    scope.data.personDate = "10.10.1980"
    scope.data.contacts = [ {contactType : -15, contactValue : "skype contact"} ]
    scope.data.offers = [offerName : "can do something"]
    scope.data.orgs = [orgName : "person's org"]
    scope.data.tags = [label : "some new tag"]

    $httpBackend.expectPOST(baseUrl + 'people', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

  it "when new perosn linked directly to profile", ->

    expected =
      "label":"max"
      "nameParts" : "firstName" : "max"
      "birthDate":null
      "sex":null
      "contacts":[]
      "people":[tag : {id: -39}, hostTag : {id: -39}]
      "organizations":[]
      "offers":[]
      "tags":[]

    modalInstance = open template : "<form name='data.personModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.name = fullName : 'max', firstName : 'max'
    scope.data.relationType = "direct"
    scope.data.personRelType = id : -39
    scope.data.ownerRelType = id : -39

    $httpBackend.expectPOST(baseUrl + 'people', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

  it "when new perosn linked to profile via proxy", ->

    expected =
      "label":"max"
      "nameParts" : "firstName" : "max"
      "birthDate":null
      "sex":null
      "contacts":[]
      "people":[tag : {id : -39}, hostTag : {id : -39}, proxy : label : "Proxy Pesron"]
      "organizations":[]
      "offers":[]
      "tags":[]

    modalInstance = open template : "<form name='data.personModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead

    scope.data.name = fullName : 'max', firstName : 'max'
    scope.data.relationType = "proxy"
    scope.data.relationProxyLabel = "Proxy Pesron"
    scope.data.personRelType.name = "знакомый"
    scope.data.ownerRelType.name = "знакомый"

    $httpBackend.expectPOST(baseUrl + 'people', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()
