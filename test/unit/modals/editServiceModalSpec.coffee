#modal test examples `https://github.com/angular-ui/bootstrap/blob/master/src/modal/test/modal.spec.js`

xdescribe "When fields in `Create new offer modal` is set, we should send to offer correct data", ->

  baseUrl = null
  offerModal = null
  $rootScope = null
  $httpBackend = null

  open = (modalOptions) ->
    modal = offerModal.open(modalOptions);
    $rootScope.$digest()
    modal

  beforeEach(module("qlanir-app"))

  beforeEach inject (_offerModal_, _$rootScope_, _$httpBackend_, webApiConfig) ->
    $rootScope = _$rootScope_
    $httpBackend = _$httpBackend_
    offerModal = _offerModal_
    baseUrl = webApiConfig.url

  it "when scope's fields are set with out proxy", ->

    expected =
      description: "test offer description"
      type:
        label: "test"
      linkedSubject:
        label: "test person"
        type: "person"


    modalInstance = open template : "<form name='data.offerModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead
    scope.data.offerType =
        label: "test"
    scope.data.hostSubject =
        label: "test person"
        type: "person"
    scope.data.offerDescription = "test offer description"
    scope.data.isProxy = false

    $httpBackend.expectPOST(baseUrl + 'offers', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()

  it "when scope's fields are set with proxy", ->

    expected =
      description: "test offer description"
      type:
        label: "test"
      linkedSubject:
        label: "test org"
        type: "organization"
      proxy:
        label: "proxy person"
        type: "person"


    modalInstance = open template : "<form name='data.offerModal' novalidate></form>"

    #FG: see tests of bootstarp.ui something wrong here !
    scope = $rootScope.$$childHead
    scope.data.offerType =
      label: "test"
    scope.data.hostSubject =
      label: "proxy person"
      type: "person"
    scope.data.proxySubject =
      label: "test org"
      type: "organization"
    scope.data.offerDescription = "test offer description"
    scope.data.isProxy = true

    $httpBackend.expectPOST(baseUrl + 'offers', expected).respond(200, '')
    scope.saveAndExit()
    $httpBackend.flush()

    $httpBackend.verifyNoOutstandingExpectation()