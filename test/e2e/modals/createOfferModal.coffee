"use strict"

module.exports = class CreateOfferModal

  constructor: ->

  get : ->
    browser.get("")

  openModal: ->
    element(By.css('[ng-click="newOffer()"]')).click()

  openModalForEdit: -> #на экране должен быть только один открытый offerItem
    element(By.css('[ng-click="editOffer(item)"]')).click()

  closeModal: ->
    element(By.css('form.addOffer a.close')).click()

  setFieldVal: (fieldId, val) ->
    element(By.css("##{fieldId}")).sendKeys(val)

  clickField: (fieldId) ->
    element(By.css("##{fieldId}")).click()

  saveAndExit: ->
    element(By.css('[name="data.offerModal"] [ng-click="saveAndExit()"]')).click()

  reset: ->
    element(By.css('[name="data.offerModal"] [ng-click="reset()"]')).click()

  selectDropdown: ( optionId, optionNum ) ->
    if optionNum
      options = By.css("##{optionId}").findElements(By.tagName('option'))
      .then (options) ->
        options[optionNum].click()
