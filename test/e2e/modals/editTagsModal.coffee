"use strict"

module.exports = class EditTagsModal

  constructor: ->

  get : ->
    browser.get("")

  openModal: ->
    element(By.css('[ng-click="editTags()"]')).click()

  closeModal: ->
    element(By.css('form.tags a.close')).click()

  updFieldVal: (fieldId, val) ->
    field = element(By.css("##{fieldId}"))
    field.clear()
    field.sendKeys(val)

  setFieldVal: (fieldId, val) ->
    element(By.css("##{fieldId}")).sendKeys(val)

  clickField: (fieldId) ->
    element(By.css("##{fieldId}")).click()


  selectDropdown: ( optionId, optionNum ) ->
    if optionNum
      options = By.css("##{optionId}").findElements(By.tagName('option'))
      .then (options) ->
        options[optionNum].click()
