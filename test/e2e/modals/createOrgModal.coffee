"use strict"

module.exports = class CreateOrgModal

  constructor: ->

  get : ->
    browser.get("")

  openModal: ->
    element(By.css('[ng-click="newOrg()"]')).click()

  closeModal: ->
    element(By.css('form.addOrganization a.close')).click()

  updFieldVal: (fieldId, val) ->
    field = element(By.css("##{fieldId}"))
    field.clear()
    field.sendKeys(val)

  setFieldVal: (fieldId, val) ->
    element(By.css("##{fieldId}")).sendKeys(val)

  clickField: (fieldId) ->
    element(By.css("##{fieldId}")).click()

  saveAndExit: ->
    element(By.css('[name="data.orgModal"] [ng-click="saveAndExit()"]')).click()

  reset: ->
    element(By.css('[name="data.orgModal"] [ng-click="reset()"]')).click()

  selectDropdown: ( optionId, optionNum ) ->
    if optionNum
      options = By.css("##{optionId}").findElements(By.tagName('option'))
      .then (options) ->
        options[optionNum].click()
