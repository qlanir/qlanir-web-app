"use strict"

exports.config =
  baseUrl: 'http://devil.goip.ru:8012'
  seleniumAddress: "http://localhost:4444/wd/hub"
  specs: ["scenarios/**/*.coffee"]
  capabilities:
    'browserName': 'chrome'
  allScriptsTimeout : 100000
  jasmineNodeOpts:
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: false
    defaultTimeoutInterval: 50000

