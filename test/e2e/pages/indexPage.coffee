"use strict"

config = require("yaml-config").readConfig('./app.yml', process.env.NODE_ENV)

request = require "request"

module.exports = class IndexPage

  constructor: ->

    @termInput = $('#term_input')
    @searchButton = $("#search_button")
    @searchResults = $("#search_results")
    @meadowItems = $$(".tt-meadow-item")
    @personFilterToggle = $("#person_filter_toggle")
    @orgFilterToggle = $("#org_filter_toggle")
    @serviceFilterToggle = $("#service_filter_toggle")
    @history = $(".tt-history")

  clean: ->

    d = protractor.promise.defer()

    request config.e2e.cleanUrl, (err, res) ->
      console.log "clean", config.e2e.cleanUrl, err
      if !err and res.statusCode == 200
        d.fulfill()
      else
        browser.close()
        d.reject(err || res.statusCode)

    d.promise


  getMeadowSearchItemResults: (meadowSearchItem) ->
    meadowSearchItem.$$(".tt-meadow-item-child")

  get : ->
    browser.get ""

  search: (term) ->
    browser.actions().doubleClick(@termInput).perform()
    @termInput.sendKeys(term)
    @searchButton.click()

  pinItem: (issuerNumber) ->
    pinButton = element.all(By.css(".tt-meadow-item")).get(issuerNumber - 1).all(By.css(".pin"))[0]
    pinButton.click()


  getItem: (number) ->
    $(".tt-meadow-item:nth-of-type(" + number + ")")

  getItemContentText: (issuerNumber, groupNumber) ->
    #$(".tt-meadow-item:nth-of-type(" + number + ") .tt-meadow-item-group").getText()
    if groupNumber
      element.all(By.css(".tt-meadow-item")).get(issuerNumber - 1)
      .all(By.css(".tt-meadow-item-group")).get(groupNumber - 1).getText()
    else
      element.all(By.css(".tt-meadow-item")).get(issuerNumber - 1)
      .all(By.css(".tt-meadow-item-group"))
      .reduce((k, elem) -> elem.getText().then((txt) ->
          txt = txt.trim() if txt
          if k then (if !!txt then k + "\n" + txt.trim() else k) else txt))


  clickItem: (issuerNumber, groupNumber, subjectNumber) ->
    #s = ".tt-meadow-item:nth-of-type(#{issuerNumber}) .tt-meadow-item-group:nth-of-type(#{groupNumber}) .tt-meadow-item-child:nth-of-type(#{subjectNumber})"
    #group = element.all(By.css(".tt-meadow-item:nth-of-type(#{issuerNumber})")).get(groupNumber)
    #s = ".tt-meadow-item:eq(#{issuerNumber - 1}) .tt-meadow-item-group:eq(#{groupNumber - 1}) .tt-meadow-item-child:eq(#{subjectNumber - 1})"
    item = element.all(By.css(".tt-meadow-item")).get(issuerNumber - 1)
    .all(By.css(".tt-meadow-item-group")).get(groupNumber - 1)
    .all(By.css(".tt-meadow-item-child")).get(subjectNumber - 1)
    item.click()

  clickHistory: (historyNumber) ->
    s = ".tt-history-item:nth-of-type(#{historyNumber})"
    item = $(s)
    item.click()

  openEditModal: (itemIndex) ->
    itemIndex ?= 1
    item = element.all(By.css(".tt-meadow-item")).get(itemIndex).all(By.css("a.edit")).get(0)
    #browser.actions().mouseMove(item).perform()
    item.click()
