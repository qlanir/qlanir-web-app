"use strict"

exports.config =
  baseUrl: 'http://localhost:8020'
  seleniumAddress: "http://localhost:4444/wd/hub"
  specs: ["scenarios/**/*-e2e.coffee"]
  capabilities:
    'browserName': 'chrome'
  allScriptsTimeout : 100000
  jasmineNodeOpts:
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: false
    defaultTimeoutInterval: 50000

