"use strict"

IndexPage = require "../../pages/indexPage"
CreateOrgModal = require "../../modals/createOrgModal"

describe "Create new org via CreateOrgModal", ->

  indexPage = new IndexPage()
  modal = new CreateOrgModal()
  indexPage.get()

  it "clen db", ->
    flow = protractor.promise.controlFlow()
    flow.execute(indexPage.clean)

  describe "create org `simple org` only with name", ->

    describe "open `create org` modal, insert person name and save should work without errors", ->

      it "open modal", ->
        modal.openModal()
        expect($('[name="data.orgModal"]').isDisplayed()).toBeTruthy()

      it "fill in org name and save", ->
        modal.setFieldVal("orgName", "simple org")
        modal.saveAndExit()
        expect(browser.isElementPresent(By.css('[name="data.orgModal"]'))).toBeFalsy()

    describe "find and open `simple org`", ->

      it "`simple org` should be found in search", ->

        indexPage.search "simple"
        $("#searchOrg").click()

        expected = """
        simple org
        """

        expect(indexPage.meadowItems.count()).toEqual(1)
        expect(indexPage.getItemContentText(1)).toEqual(expected)

      it "`simple org` should be displayed as item when clicked in serach result", ->

        indexPage.clickItem 1, 1, 1

        expected = """
        simple org
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected)


  xdescribe "create org with all fields", ->

    describe "open `org person` modal, fill in fields and save should work without errors", ->

      it "open modal", ->
        modal.openModal()
        expect($('[name="data.orgModal"]').isDisplayed()).toBeTruthy()

      it "fill in fields and save", ->

        #person name
        modal.setFieldVal("orgName", "org full")

        #contact
        #select google contact type
        $('form.addOrganization  select#relationFor option:nth-child(2)').click()
        modal.setFieldVal("noteValue", "org.full@gmail.com")

        #service
        #modal.setFieldVal("orgService", "org full service")

        #person's org
        modal.setFieldVal("orgPerson", "person full")
        $('form.addOrganization select#relationFor option:nth-child(3)').click()

        #tags

        #TODO : problem with testing 2 tags in a row, since then they are read from html, unknown line separator doesn't match expected sep
        $("#orgTag .tags").click()
        $("#orgTag input.input").sendKeys("tag1")
        $("#orgTag input.input").sendKeys(protractor.Key.ENTER)

        modal.saveAndExit()

        #check if closed
        expect(browser.isElementPresent(By.css('[name="data.orgModal"]'))).toBeFalsy()

    describe "find and open `org full`", ->

      it "`org full` should be found in search", ->

        indexPage.search "full"
        $("#searchOrg").click()

        expected = """
          org full
          person full
        """
        
        expect(indexPage.meadowItems.count()).toEqual(1)
        expect(indexPage.getItemContentText(1)).toEqual(expected)

      it "`org full` should be displayed as item when clicked in serach result", ->

        indexPage.clickItem 1, 1, 1

        expected = """
        org full
        org.full@gmail.com
        person full-учредитель
        tag1
        """

        expect(indexPage.meadowItems.count()).toEqual(2)

        actual = indexPage.getItemContentText(2)

        expect(actual).toEqual(expected)

      describe "open modal to edit `org full`", ->

        it "open `org full` modal", ->

          indexPage.openEditModal 1
          expect($('[name="data.orgModal"]').isDisplayed()).toBeTruthy()

          #TODO : check select options and tags
          expect($("#orgName").getAttribute("value")).toEqual("org full")
          #expect(protractor.By.selectedOption("contact.contactType").getText()).toEqual("google contact")
          expect($("#noteValue").getAttribute("value")).toEqual("org.full@gmail.com")
          #expect($("#orgService").getAttribute("value")).toEqual("org full service")
          expect($("#orgPerson").getAttribute("value")).toEqual("person full")

          expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button[disabled]'))).toBeTruthy()
          expect(browser.isElementPresent(By.css('form.addOrganization input.button.reset-button[disabled]'))).toBeTruthy()

        it "modify fields and save of `org full`", ->

          #org name
          modal.updFieldVal("orgName", "org full updated")

          #contact
          #select mail contact type
          $('form.addOrganization  select#relationFor option:nth-child(2)').click()
          modal.updFieldVal("noteValue", "org.full.updated@gmail.com")

          #service
          #modal.updFieldVal("orgService", "org full service updated")

          #person's org (владелец)
          modal.updFieldVal("orgPerson", "contact updated")
          $('form.addOrganization select#relationFor option:nth-child(2)').click()

          #tags

          #TODO : problem with testing 2 tags in a row, since then they are read from html, unknown line separator doesn't match expected sep
          #remove and add new tag
          ###
          $("#orgTag .tags").click()
          $("#orgTag input.input").sendKeys("tag1")
          $("#orgTag input.input").sendKeys(protractor.Key.ENTER)
          ###
          modal.saveAndExit()

          #check if closed
          expect(browser.isElementPresent(By.css('[name="data.orgModal"]'))).toBeFalsy()

      describe "find and open `org full updated`", ->

          it "`org full updated` should be found in search", ->

            indexPage.search "full"
            $("#searchOrg").click()

            expected = """
              org full updated
            """

            expect(indexPage.meadowItems.count()).toEqual(1)
            expect(indexPage.getItemContentText(1)).toEqual(expected)

          it "`org full updated` should be displayed as item when clicked in serach result", ->

            indexPage.clickItem 1, 1, 1

            expected = """
            org full updated
            org.full.updated@gmail.com
            contact updated-владелец
            tag1
            """

            expect(indexPage.meadowItems.count()).toEqual(2)

            actual = indexPage.getItemContentText(2)

            expect(actual).toEqual(expected)

