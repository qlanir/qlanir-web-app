"use strict"

IndexPage = require "../../pages/indexPage"
CreateOrgModal = require "../../modals/createOrgModal"

describe "Create linked subjects via CreateOrgModal", ->

  indexPage = new IndexPage()
  modal = new CreateOrgModal()
  indexPage.get()

  it "clean db", ->
    flow = protractor.promise.controlFlow()
    flow.execute(indexPage.clean)

  it "create org `org with person` with linked person `person`", ->

    modal.openModal()
    modal.setFieldVal("orgName", "org with person")

    #person's org (владелец)
    modal.setFieldVal("orgPerson", "person")
    $('form.addOrganization select#relationFor option:nth-child(2)').click()

    modal.saveAndExit()
    expect(browser.isElementPresent(By.css('[name="data.personModal"]'))).toBeFalsy()

  it "create org `org with person` with the same linked person `person`", ->

    modal.openModal()
    modal.setFieldVal("orgName", "org with the same person")

    #person's org (владелец)

    modal.setFieldVal("orgPerson", "per")
    $('[ng-repeat="person in data.people"] li:nth-of-type(1)').click()
    $('form.addOrganization select#relationFor option:nth-child(2)').click()

    modal.saveAndExit()
    expect(browser.isElementPresent(By.css('[name="data.personModal"]'))).toBeFalsy()

  it "find person `person` by filter", ->

    indexPage.search "person"
    $("#searchPerson").click()

    expected = """
          person
          org with person
          org with the same person
        """

    expect(indexPage.meadowItems.count()).toEqual(1)
    expect(indexPage.getItemContentText(1)).toEqual(expected)
