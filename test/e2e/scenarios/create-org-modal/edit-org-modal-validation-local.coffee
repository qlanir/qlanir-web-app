"use strict"

CreateOrgModal = require "../../modals/createOrgModal"

describe "CreateOrgModal validation and errors", ->

  modal = new CreateOrgModal()
  modal.get()

  it "when open modal it should appear", ->
    modal.openModal()
    expect($('[name="data.orgModal"]').isDisplayed()).toBeTruthy()

  it "submit and reset buttons should be disabled when required fields are not filled", ->
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.reset-button[disabled]'))).toBeTruthy()

  it "submit and reset buttons should not be disabled when required fields are filled", ->
    modal.setFieldVal("orgName", "test test")
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button[disabled]'))).toBeFalsy()

  it "submit and reset buttons should be disabled after reset", ->
    modal.reset()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.reset-button[disabled]'))).toBeTruthy()

  it "fill in and then delete `org name`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.setFieldVal("orgName", "test org")
    $("#orgName").clear()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOrganization input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addOrganization .org-name-validation-error").isDisplayed()).toBeTruthy()


  it "when user clcik `close button` form should dissapear", ->
    modal.closeModal()
    expect(browser.isElementPresent(By.css('form.addOrganization'))).toBeFalsy()
