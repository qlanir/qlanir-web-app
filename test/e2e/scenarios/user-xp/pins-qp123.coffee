"use strict"
###
IndexPage = require "../../pages/indexPage"

describe "Create subject, pin it two times, then remove subject", ->
  indexPage = new IndexPage()
  indexPage.get()

  modal = new CreatePersonModal()
  indexPage.get()

  describe "create subject", ->

      it "open modal", ->
        modal.openModal()
        expect($('[name="data.personModal"]').isDisplayed()).toBeTruthy()

      it "fill in person name and save", ->
        modal.setFieldVal("personName", "simple person")
        modal.saveAndExit()
        expect(browser.isElementPresent(By.css('[name="data.personModal"]'))).toBeFalsy()

  describe "navigate to subject", ->

    it "find subject", ->

      indexPage.search "simple"
      $("#searchPerson").click()

      expected = """
      simple person
      """

      expect(indexPage.meadowItems.count()).toEqual(1)
      expect(indexPage.getItemContentText(1)).toEqual(expected)

    it "navigate subject", ->

      indexPage.clickItem 1, 1, 1

      expected = """
      simple person
      """

      expect(indexPage.meadowItems.count()).toEqual(2)
      expect(indexPage.getItemContentText(2)).toEqual(expected)

  descrie "pin subject", ->

    it "pin item first time", ->
      indexPage.pinItem 2
      indexPage.setPinText 1, "first"

    it "pin item second time", ->
      indexPage.pinItem 2
      indexPage.setPinText 2, "second"

  descrie "rename subject", ->

    it "open modal for edit", ->
      indexPage.openEditModal 1
      expect($('[name="data.personModal"]').isDisplayed()).toBeTruthy()
      modal.setFieldVal("personName", "upd person")

    it "pins subject must be updated", ->
      expect(indexPage.getPinItemCaption(1)).toEqual("upd person")
      expect(indexPage.getPinItemCaption(2)).toEqual("upd person")

###