"use strict"

CreateOfferModal = require "../../modals/createOfferModal"

describe "CreateOfferModal validation and errors", ->

  modal = new CreateOfferModal()
  modal.get()

  it "when open modal it should appear", ->
    modal.openModal()
    expect($('[name="data.offerModal"]').isDisplayed()).toBeTruthy()

  it "submit and reset buttons should be disabled when required fields are not filled", ->
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.reset-button[disabled]'))).toBeTruthy()

  it "submit and reset buttons should not be disabled when required fields are filled", ->
    modal.setFieldVal("offerName", "test offer")
    modal.setFieldVal("offerContact", "test contact")
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button[disabled]'))).toBeFalsy()

  it "submit and reset buttons should be disabled after reset", ->
    modal.reset()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.reset-button[disabled]'))).toBeTruthy()

  it "fill in and then delete `offer name`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.setFieldVal("offerName", "test offer")
    $("#offerName").clear()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addOffer .offer-name-validation-error").isDisplayed()).toBeTruthy()

  it "fill in and then delete `offer contact`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.setFieldVal("offerContact", "test contact")
    $("#offerContact").clear()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addOffer input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addOffer .offer-contact-validation-error").isDisplayed()).toBeTruthy()

  it "when user clcik `close button` form should dissapear", ->
    modal.closeModal()
    expect(browser.isElementPresent(By.css('form.addOffer'))).toBeFalsy()
