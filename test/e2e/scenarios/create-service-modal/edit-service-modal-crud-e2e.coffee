"use strict"

IndexPage = require "../../pages/indexPage"
CreateOfferModal = require "../../modals/createOfferModal"

describe "Create new offer via CreateOfferModal", ->

  indexPage = new IndexPage()
  modal = new CreateOfferModal()
  indexPage.get()

  it "clen db", ->
    flow = protractor.promise.controlFlow()
    flow.execute(indexPage.clean)

  describe "create offer without proxy", ->

    describe "open `create offer` modal, insert all fields exclude proxy and save should work without errors", ->

      it "open modal", ->
        modal.openModal()
        expect($('[name="data.offerModal"]').isDisplayed()).toBeTruthy()

      it "fill all fields exlude proxy and save", ->
        modal.setFieldVal("offerName", "test offer")
        modal.setFieldVal("offerContact", "offer contact person")
        modal.setFieldVal("offerInfo", "test offer description")
        modal.saveAndExit()
        expect(browser.isElementPresent(By.css('[name="data.offerModal"]'))).toBeFalsy()

    describe "find and open `test offer`", ->

      it "`test offer` should be found in search", ->

        indexPage.search "offer"
        $("#searchOffer").click()

        expected = """
        test offer
        offer contact person
        test offer description
        """

        expect(indexPage.meadowItems.count()).toEqual(1)
        expect(indexPage.getItemContentText(1)).toEqual(expected)

      ###it "test offer` group should be displayed as item when clicked in serach result", ->

        indexPage.clickItem 1, 1, 1

        expected = """
        offer contact person
        test offer description
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected) QL-126###

      it "test offer` should be displayed as item when clicked in group", ->

        indexPage.clickItem 1, 1, 1 #indexPage.clickItem 2, 1, 1 QL-126

        expected = """
        test offer
        test offer description
        offer contact person
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected)

  describe "create offer with proxy and edit", ->

    describe "open `create offer` modal, insert all and save should work without errors", ->

      it "open modal", ->
        modal.openModal()
        expect($('[name="data.offerModal"]').isDisplayed()).toBeTruthy()

      it "fill all fields and save", ->
        modal.setFieldVal("offerName", "new offer")
        modal.setFieldVal("offerContact", "proxy person")
        modal.clickField("offerRel2Label") #Show offerPerformer input
        modal.setFieldVal("offerPerformer", "offer contact org2")
        modal.clickField("offerPerformerType2Label")
        modal.setFieldVal("offerInfo", "offer with proxy description")
        modal.saveAndExit()
        expect(browser.isElementPresent(By.css('[name="data.offerModal"]'))).toBeFalsy()

    describe "find and open `new offer`", ->

      it "`new offer` should be found in search", ->

        indexPage.search "offer"
        $("#searchOffer").click()

        expected = """
          new offer
          offer contact org2
          offer with proxy description
          test offer
          offer contact person
          test offer description
        """

        expect(indexPage.meadowItems.count()).toEqual(1)
        expect(indexPage.getItemContentText(1)).toEqual(expected)

      ###it "new offer` group should be displayed as item when clicked in serach result", ->

        indexPage.clickItem 1, 1, 1

        expected = """
        offer contact person2
        offer with proxy description
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected) QL-126###

      it "new offer` should be displayed as item when clicked in group", ->

        indexPage.clickItem 1, 1, 1 #indexPage.clickItem 2, 1, 1 QL-126

        expected = """
        new offer
        offer with proxy description
        offer contact org2
        proxy person
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected)

    describe "open `edit offer` modal and update 'new offer'", ->

      it "open modal", ->
        modal.openModalForEdit()
        #indexPage.openEditModal 2
        expect($('[name="data.offerModal"]').isDisplayed()).toBeTruthy()

      it "edit fields and save", ->
        modal.setFieldVal("offerInfo", " (updated)")
        modal.setFieldVal("offerPerformer", ".1")
        modal.clickField("offerPerformerType2Label")
        #modal.clickField("offerRel1Label") #Hide offerPerformer input
        modal.saveAndExit()
        expect(browser.isElementPresent(By.css('[name="data.offerModal"]'))).toBeFalsy()

      it "new offer` should be updated", ->

        expected = """
        new offer
        offer with proxy description (updated)
        offer contact org2.1
        proxy person
        """

        expect(indexPage.meadowItems.count()).toEqual(2)
        expect(indexPage.getItemContentText(2)).toEqual(expected)