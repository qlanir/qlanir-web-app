"use strict"

IndexPage = require "../pages/indexPage"

xdescribe "search and sweetch between results test", ->

  indexPage = null
  indexPage = new IndexPage()
  indexPage.get()

  it "search items", ->
    indexPage.search("abc")

  it "search result should contain proper items", ->
    expect(indexPage.searchResults.getText()).toEqual("""
      Персоны
      3
      Услуги
      0
      Организации
      1""")

  it "meadow should contain 1 item", ->
    expect(indexPage.meadowItems.count()).toEqual(1)

  it "person toggle should be active", ->
    expect(indexPage.personFilterToggle.getAttribute("class")).toMatch("active")
    expect(indexPage.orgFilterToggle.getAttribute("class")).not.toMatch("active")
    expect(indexPage.serviceFilterToggle.getAttribute("class")).not.toMatch("active")

  it "meadow search item (people) should contain 3 results", ->
    searchItem = indexPage.meadowItems.first()
    indexPage.getMeadowSearchItemResults(searchItem).then (res) ->
      expect(res.length).toEqual(3)

  it "click by [org] filter", ->
    indexPage.orgFilterToggle.click()

  it "org toggle should be active", ->
    expect(indexPage.personFilterToggle.getAttribute("class")).not.toMatch("active")
    expect(indexPage.orgFilterToggle.getAttribute("class")).toMatch("active")
    expect(indexPage.serviceFilterToggle.getAttribute("class")).not.toMatch("active")

  it "meadow search item (orgs) should contain 1 result", ->
    searchItem = indexPage.meadowItems.first()
    indexPage.getMeadowSearchItemResults(searchItem).then (res) ->
      expect(res.length).toEqual(1)

  it "click by [services] filter", ->
    indexPage.serviceFilterToggle.click()

  it "service toggle should be active", ->
    expect(indexPage.personFilterToggle.getAttribute("class")).not.toMatch("active")
    expect(indexPage.orgFilterToggle.getAttribute("class")).not.toMatch("active")
    expect(indexPage.serviceFilterToggle.getAttribute("class")).toMatch("active")

  it "meadow search item (service) should contain 0 results", ->
    searchItem = indexPage.meadowItems.first()
    indexPage.getMeadowSearchItemResults(searchItem).then (res) ->
      expect(res.length).toEqual(0)


