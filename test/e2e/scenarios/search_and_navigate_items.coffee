"use strict"

IndexPage = require "../pages/indexPage"

xdescribe "search and sweetch between results test", ->

  indexPage = null
  indexPage = new IndexPage()
  indexPage.get()

  describe "search 'term'", ->

    it "history should contain 'term' item", ->
      indexPage.search("term")
      expect(indexPage.history.getText()).toEqual("term")

  describe "open subj 1", ->

    it "history should contain 'term / subj-1' item", ->
      indexPage.clickItem(1, 1, 1)
      expect(indexPage.history.getText()).toEqual("termsubj-1")

    it "meadow should contain 2 items", ->
      expect(indexPage.meadowItems.count()).toEqual(2)

  describe "open subj 2 from subj 1", ->

    it "history should contain 'term / subj-1 / subj-2' item", ->
      indexPage.clickItem(2, 3, 1)
      expect(indexPage.history.getText()).toEqual("termsubj-1subj-2")

    it "meadow should contain 3 items", ->
      expect(indexPage.meadowItems.count()).toEqual(3)

  describe "open subj 2 from search", ->

    it "history should contain 'term / subj-1 / subj-2 / subj-2' item", ->
      indexPage.clickItem(1, 1, 2)
      expect(indexPage.history.getText()).toEqual("termsubj-1subj-2subj-2")

    it "meadow should contain 2 items", ->
      expect(indexPage.meadowItems.count()).toEqual(2)

  describe "open subj 2 (first) from history", ->

    it "history should contain 'term / subj-1 / subj-2 / subj-2' item", ->
      indexPage.clickHistory(3)
      expect(indexPage.history.getText()).toEqual("termsubj-1subj-2subj-2")

    it "meadow should contain 3 items", ->
      expect(indexPage.meadowItems.count()).toEqual(3)
