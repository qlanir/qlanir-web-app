"use strict"

IndexPage = require "../../pages/indexPage"
EditTagsModal = require "../../modals/editTagsModal"

xdescribe "Tags modal create / upadte /delete tags", ->
  indexPage = new IndexPage()
  modal = new EditTagsModal()
  indexPage.get()

  it "clean db", ->
    flow = protractor.promise.controlFlow()
    flow.execute(indexPage.clean)

  it "open modal", ->
    modal.openModal()
    expect($('form.tags').isDisplayed()).toBeTruthy()

  describe "crete tag", ->
    it "create `test` tag via input", ->
      modal.setFieldVal("tag", "test")
      $('[ng-click="createTag(inputTag)"]').click()

    it "tag `test` should appear in `found tags` area", ->
      #TODO : sometimes es returns twitter, vkontakte sometimes not WTF
      expect($("form.tags .group.tagList").getText()).toBe("test")

    it "create `booom` tag via input", ->
      modal.updFieldVal("tag", "boom")
      $('[ng-click="createTag(inputTag)"]').click()

    it "tag `booom` should appear in `found tags` area", ->
      expect($("form.tags .group.tagList").getText()).toBe("""boom
      facebook""")

    it "try to create `test` tag via input", ->
      modal.updFieldVal("tag", "test")
      $('[ng-click="createTag(inputTag)"]').click()

    it "only one tag `test` should appear in `found tags` area", ->
      expect($("form.tags .group.tagList").getText()).toBe("test")

    describe "find tags", ->
      it "find unexistent tag", ->
        modal.updFieldVal("tag", "unexistent")
        expect($("form.tags .group.tagList").getText()).toBe("")

      it "find `tezt` tag", ->
        modal.updFieldVal("tag", "tezt")
        expect($("form.tags .group.tagList").getText()).toBe("test")

    describe "remove tag", ->
      describe "ask remove tag", ->
        describe "click `ask remove` and then cancel `remove`", ->
          it "click remove tag link", ->
            element.all(By.css("div.tagList a.delete")).get(0).click()

          it "confirmation message should be shown", ->
            expect($('span[ng-show="removingTag"]').isDisplayed()).toBeTruthy()
            expect($('span[ng-show="removingTag"] div.warning span.italic').getText()).toBe("(привязана к 0 объeктам)")

          it "click `cancel` remove", ->
            $('form.tags .button.cancel').click()

          it "confirmation message should be hidden", ->
            expect($('span[ng-show="removingTag"]').isDisplayed()).toBeFalsy()

      describe "click `ask remove` and then `remove`", ->
        it "click remove tag link, again", ->
          element.all(By.css("div.tagList a.delete")).get(0).click()

        it "confirmation message should be shown", ->
          expect($('span[ng-show="removingTag"]').isDisplayed()).toBeTruthy()
          expect($('span[ng-show="removingTag"] div.warning span.italic').getText()).toBe("(привязана к 0 объeктам)")

        it "click remove button", ->
          $('form.tags .button.delete').click()

        it "confirmation message should be hidden", ->
          expect($('span[ng-show="removingTag"]').isDisplayed()).toBeFalsy()

        it "none tags should appear in `found tags` area", ->
          expect($("form.tags .group.tagList").getText()).toBe("")

        it "find `test` tag should find nothing", ->
          #TODO : sometimes es returns twitter, vkontakte sometimes not WTF
          modal.updFieldVal("tag", "test")
          expect($("form.tags .group.tagList").getText()).toBe("")

  it "close modal", ->
    modal.closeModal()
    expect(browser.isElementPresent(By.css('form.tags'))).toBeFalsy()
