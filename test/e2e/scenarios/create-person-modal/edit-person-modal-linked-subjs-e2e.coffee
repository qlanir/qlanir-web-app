"use strict"

IndexPage = require "../../pages/indexPage"
CreatePersonModal = require "../../modals/createPersonModal"

describe "Create linked subjects via CreatePersonModal", ->

  indexPage = new IndexPage()
  modal = new CreatePersonModal()
  indexPage.get()

  it "clean db", ->
    flow = protractor.promise.controlFlow()
    flow.execute(indexPage.clean)

  it "create person `person with org` with linked organization `org`", ->

    modal.openModal()
    modal.setFieldVal("personName", "person with org")
    modal.setFieldVal("personOrg", "person org")
    modal.saveAndExit()
    expect(browser.isElementPresent(By.css('[name="data.personModal"]'))).toBeFalsy()

  it "create person `person with the same org` with linked organization `org`", ->

    modal.openModal()
    modal.setFieldVal("personName", "person with the same org")

    modal.setFieldVal("personOrg", "per")
    $('[ng-repeat="org in data.orgs"] li:nth-of-type(1)', "per").click()

    modal.saveAndExit()
    expect(browser.isElementPresent(By.css('[name="data.personModal"]'))).toBeFalsy()


  it "find org `person org` by filter", ->

    indexPage.search "org"
    $("#searchOrg").click()

    expected = """
          person org
          person with org
          person with the same org
        """

    expect(indexPage.meadowItems.count()).toEqual(1)
    expect(indexPage.getItemContentText(1)).toEqual(expected)
