"use strict"

IndexPage = require "../../pages/indexPage"
CreatePersonModal = require "../../modals/createPersonModal"

describe "CreatePersonModal intercation", ->

  modal = new CreatePersonModal()
  modal.get()

  it "when open modal it should appear", ->
    modal.openModal()
    expect($('[name="data.personModal"]').isDisplayed()).toBeTruthy()

  it "default field values and visibility should be as expected", ->
    #TODO : check default values for select !
    expect($('form.addPerson #personName').getAttribute("value")).toEqual("")

    expect($('#relationFor').isDisplayed()).toBeFalsy()
    #expect($('#relationTo').isDisplayed()).toBeFalsy()
    expect($('#personPerformer').isDisplayed()).toBeFalsy()

  describe "when user clcik `use direct link` - fields `role` and `profile role` should be visible and `proxy` is not", ->

    it "when `direct relation` checkbox clicked the `roles fields` should became visible and `proxy` is not", ->
      modal.clickField("personRel1Label")
      expect($('#relationFor').isDisplayed()).toBeTruthy()
      #expect($('#relationTo').isDisplayed()).toBeTruthy()
      expect($('#personPerformer').isDisplayed()).toBeFalsy()

    it "when `direct relation` checkbox clicked again `roles fields` and `proxy` should became hidden", ->
      modal.clickField("personRel1Label")
      expect($('#relationFor').isDisplayed()).toBeFalsy()
      #expect($('#relationTo').isDisplayed()).toBeFalsy()
      expect($('#personPerformer').isDisplayed()).toBeFalsy()

  describe "when user clcik `use proxy link` - fields `role` and `profile role` and `proxy` should be visible", ->

    it "when `proxy relation` checkbox clicked the `roles fields` and `proxy` should became visible", ->
      modal.clickField("personRel2Label")
      expect($('#relationFor').isDisplayed()).toBeTruthy()
      #expect($('#relationTo').isDisplayed()).toBeTruthy()
      expect($('#personPerformer').isDisplayed()).toBeTruthy()

    it "when `direct relation` checkbox clicked again `roles fields` and `proxy` should became hidden", ->
      modal.clickField("personRel2Label")
      expect($('#relationFor').isDisplayed()).toBeFalsy()
      #expect($('#relationTo').isDisplayed()).toBeFalsy()
      expect($('#personPerformer').isDisplayed()).toBeFalsy()

  it "reset buton should reset fields but, not close form", ->

    modal.setFieldVal("personName", "test")
    modal.clickField("personRel2Label")
    expect($('form.addPerson #personName').getAttribute("value")).toEqual("test")
    expect($('#relationFor').isDisplayed()).toBeTruthy()
    #expect($('#relationTo').isDisplayed()).toBeTruthy()
    expect($('#personPerformer').isDisplayed()).toBeTruthy()

    modal.reset()

    expect($('form.addPerson #personName').getAttribute("value")).toEqual("")
    expect($('#relationFor').isDisplayed()).toBeFalsy()
    #expect($('#relationTo').isDisplayed()).toBeFalsy()
    expect($('#personPerformer').isDisplayed()).toBeFalsy()

  it "when user clcik `close button` form should dissapear", ->
    modal.closeModal()
    expect(browser.isElementPresent(By.css('form.addPerson'))).toBeFalsy()

  describe "name fields intercation", ->

    it "open modal", ->
      modal.openModal()

    it "fill in full name field", ->
      modal.setFieldVal("personName", "last first middle")

    it "expand `name` card", ->
      modal.clickField("tt-switch-person-name-card")

    it "names on the card should be defined proerly", ->
      expect($("form.addPerson #personName").isEnabled()).toBe(false);
      expect($('form.addPerson #surname').getAttribute("value")).toEqual("last")
      expect($('form.addPerson #name').getAttribute("value")).toEqual("first")
      expect($('form.addPerson #patronymic').getAttribute("value")).toEqual("middle")
      expect($('form.addPerson #alias').getAttribute("value")).toEqual("")

    it "enter nick name", ->
      modal.setFieldVal("alias", "nick")

    it "full name shouldn't change", ->
      expect($('form.addPerson #personName').getAttribute("value")).toEqual("last first middle")

    it "change first name", ->
      modal.updFieldVal("name", "firstchanged")

    it "full name should changed", ->
      expect($('form.addPerson #personName').getAttribute("value")).toEqual("last firstchanged middle")

    it "close modal", ->
      modal.closeModal()


