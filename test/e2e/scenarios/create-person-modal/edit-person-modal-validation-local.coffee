"use strict"

CreatePersonModal = require "../../modals/createPersonModal"

describe "CreatePersonModal validation and errors", ->

  modal = new CreatePersonModal()
  modal.get()

  it "when open modal it should appear", ->
    modal.openModal()
    expect($('[name="data.personModal"]').isDisplayed()).toBeTruthy()

  it "submit and reset buttons should be disabled when required fields are not filled", ->
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.reset-button[disabled]'))).toBeTruthy()

  it "submit and reset buttons should not be disabled when required fields are filled", ->
    modal.setFieldVal("personName", "max test")
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeFalsy()

  it "submit and reset buttons should be disabled after reset", ->
    modal.reset()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.reset-button[disabled]'))).toBeTruthy()

  it "fill in and then delete `person name`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.setFieldVal("personName", "max test")
    $("#personName").clear()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addPerson .person-name-validation-error").isDisplayed()).toBeTruthy()

  it "fill in wrong `person date`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.reset()
    modal.setFieldVal("personDate", "ghjghjgkjhjk")
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addPerson .person-date-validation-error").isDisplayed()).toBeTruthy()

  # TODO : protractor sync error here
  xit "click `proxy relation check box` then fill in and clean `proxy name`, error for field should be dispalyed, buttons should be in correct state", ->
    modal.reset()
    modal.clickField("personRel2Label")
    modal.setFieldVal("personPerformer", "proxy person")
    $("#personPerformer").clear()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.submit-button[disabled]'))).toBeTruthy()
    expect(browser.isElementPresent(By.css('form.addPerson input.button.reset-button[disabled]'))).toBeFalsy()
    expect($("form.addPerson .person-performer-validation-error").isDisplayed()).toBeTruthy()

  it "when user clcik `close button` form should dissapear", ->
    modal.closeModal()
    expect(browser.isElementPresent(By.css('form.addPerson'))).toBeFalsy()
