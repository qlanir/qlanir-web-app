app.value("search_result_fixture", {
    "term" : "term",
    "totalCounts" :
    {
        "people" : 19,
        "offers" : 43,
        "orgs" : 12
    },
    "results" : [
        {
            "id" : 1,
            "name" : "subj-1",
            "type" : "person",
            "hits" : ["покраска автомобилей", "ремонт замков", "ТехЦентр"]
        },
        {
            "id" : 2,
            "name" : "subj-2",
            "type" : "person",
            "hits" : ["авто-сигнализации"]
        },
        {
            "id" : 3,
            "name" : "subj-3",
            "type" : "person",
            "hits" : ["Фаст Сервис"]
        },
        {
            "id" : 4,
            "name" : "subj-4",
            "type" : "organization",
            "hits" : ["ремонт телефонов"]
        }

    ]
})
