app.value("subject_3_fixture",
    {
        "id" : 3,
        "name": "subj-3",
        "type": "person",
        "user_link": "знакомый",
        "description": "зоомагазин Зубастик",
        "contacts": [
            {
                "type": "mobile",
                "value": "+7 (123) 456-78-90"
            },
            {
                "type": "home",
                "value": "+7 (495) 123-45-67"
            }
        ],
        "offers": [
            {
                "name": "ремонт автомобилей"
            }
        ],
        "proxy_people": [
            {
                "name": "Геннадий Ляхов",
                "whoIs": "одноклассник"
            },
            {
                "name": "Артем Коновалов",
                "whoIs": "коллега"
            }
        ],
        "organizations": [
            {
                "id": 1,
                "name": "Фаст сервис"
            }
        ],
        "links": [
            {
                "name": "Геннадий Ляхов",
                "whoIs": "тренер по боксу"
            },
            {
                "name": "Андрей Коновалов",
                "whoIs": "одногруппник по боксу"
            },
            {
                "name": "Анна Котовская",
                "whoIs": "жена"
            }
        ]
    });