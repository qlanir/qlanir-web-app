QLANIR WEB APP
================

## Project configs

###Run watchers (rebuild on source change)

`gulp dev-server --require coffee-script/register`

###Build (source : jade + coffee)

`gulp build --require coffee-script/register`


## Authorization

Project authorization use [json web token](link here) auth server.

All authentication/authorization intercation with auth-server are made via `baio-ng-cordova-auth` angular module.

When application started client send request to authorization server to load user profile.

##Run tests


Unit tests with karma, config file - `karma.conf.coffee`

Integration tests with protractor, config file e2e.conf.coffee

Protractor & web-driver must be installed globally

+ Run web-driver : `webdriver-manager start`
+ Run test services : `elastic search, neo4j`
+ Run qlanir-web-api in Test environment
+ Run qlanir-web-app in Tets environment
+ Run tests : `protractor test/e2e/e2e.conf.coffee`
