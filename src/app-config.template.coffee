"use strict"

app.constant "webApiConfig",
  url : "<%= webApiUrl %>"

app.constant "authApiConfig",
  url : "<%= authApiUrl %>"

app.constant "appConfig",
  env : "<%= env %>"
  version : "<%= version %>"
