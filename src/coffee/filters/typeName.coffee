"use strict"

app.filter "typeName", ->
  (input) ->
    switch input
      when "person" then "Персоны"
      when "organization" then "Организации"
      when "offer" then "Услуги"

