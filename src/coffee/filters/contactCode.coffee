"use strict"

app.filter "contactCode", ->
  (input) ->
    switch input
      when -12 then return "mob"
      when -13 then return "google"
      when -14 then return "eMail"
      when -603 then return "eMail"
      when -15 then return "skype"
      when -16 then return "site"
      when -17 then return "facebook"
      when -18 then return "google"
      when -19 then return "twitter"
      when -20 then return "vkontakte"
    return null
