"use strict"

angular.module('ng').filter "tel", ->

  (tel) ->

    if !tel  then return ''

    value = tel.toString().trim().replace(/^\+/, '')

    if (value.match(/[^0-9]/))
      return tel


    switch (value.length)
      when 10 # +1PPP####### -> C (PPP) ###-####
        country = 1
        city = value.slice(0, 3)
        number = value.slice(3)
        break

      when 11 # +CPPP####### -> CCC (PP) ###-####
        country = value[0]
        city = value.slice(1, 4)
        number = value.slice(4)
        break

      when 12 # +CCCPP####### -> CCC (PP) ###-####
        country = value.slice(0, 3)
        city = value.slice(3, 5)
        number = value.slice(5)
        break

      else
        return tel


    number = number.slice(0, 3) + '-' + number.slice(3)

    return (country + " (" + city + ") " + number).trim()
