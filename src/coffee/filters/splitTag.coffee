"use strict"

app.filter "splitTag", ->
  (input) ->
    s = input.split "/"
    s[s.length - 1].trim()