"use strict"

app.controller "loginController", ($scope, $rootScope, $resource, notifyService, auth, webApiConfig, itemsListService) ->

  $scope.user = null

  logout = ->
    itemsListService.swap()
    auth.openAuthService()

  $rootScope.$on BAIO_AUTH_EVENTS.logout, logout

  $rootScope.$on BAIO_AUTH_EVENTS.loginSuccess, (evt, opts) ->
    if opts
      $resource(webApiConfig.url + "user").get (res) ->
        $scope.user = res.user
        #notifyService.success("Успешный вход пользователя в систему") QP-117
        $rootScope.$broadcast "user.changed", res
      , (err) ->
        #notifyService.error("Ошибка входа пользователя в систему !") QP-117
        auth.logout()
        auth.openAuthService()
    else
      console.log ">>>login.coffee:24"
      $scope.user = user : "developer"
      notifyService.success("Тестовый вход пользователя в систему")
      $rootScope.$broadcast "user.changed", $scope.user

  $rootScope.$on BAIO_AUTH_EVENTS.loginFailed, ->
    #notifyService.error("Ошибка входа пользователя в систему") QP-117
    logout()

  $rootScope.$on BAIO_AUTH_EVENTS.forbidden, ->
    notifyService.error("Пользователь не авторизирован")
    logout()

  $scope.switchLogin = ->
    console.log "switchLogin"
    if !$scope.user
      auth.login()
    else
      $scope.user = null
      auth.logout()

app.directive "login", ->

  restrict : "E"

  templateUrl : "login.html"

  replace : true

  controller: "loginController"




