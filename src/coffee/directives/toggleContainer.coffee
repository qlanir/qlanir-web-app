app.directive "toggleContainer", ->

  restrict : 'AE'
  #scope : {}
  #transclude: true

  controller: ($scope) ->

    $scope.isToggled = false
    $scope.toggle = ->
      $scope.isToggled = !$scope.isToggled


