"use strict"

app.directive "scrollable", ($timeout) ->

  link: (scope, elem, attrs) ->

    elem.mCustomScrollbar
      horizontalScroll : true
      mouseWheel : if attrs.scrollableMouseOff then false else true
      scrollButtons :
        enable : true
      advanced :
        autoExpandHorizontalScroll : true
        autoScrollOnFocus: false

    #fix for workspace scroll

    workspaceScrollSelector = $('.workspace .mCSB_scrollTools .mCSB_draggerContainer')
    if workspaceScrollSelector[0]
      workspaceScrollSelector[0].classList.add "affix-top"

    upd = ->
      elem.mCustomScrollbar('update')
      elem.mCustomScrollbar("scrollTo", "right")

    scope.$watchCollection attrs.scrollableItems, ->
      $timeout (-> upd()), 0




