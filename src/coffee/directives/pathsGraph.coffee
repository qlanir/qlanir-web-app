app.directive "pathsGraph", ->


  restrict: "EA"

  scope:
    load: "=loadData"
    click: "&nodeClick"

  link: (scope, ele) ->

    scope.load.then (res) ->

      console.log ">>>pathsGraph.coffee:14", res

      maxPathLen = _.max res.map((m) -> m.length)
      offsetX = maxPathLen * 200 / 2

      paths = []
      for path, pi in res
        r = path.map (m, i) ->
          knownType =  (if i == 1 then "known" else "unknown") + _.capitalize m.type
          id : m.id.toString()
          label : m.label.replace(/\s/g, "\n")
          x : 0
          y : 0
          allowedToMoveX: true
          allowedToMoveY: true
          image: "img/graph/#{knownType}.svg",
          shape: 'image'
          knownType : knownType
        if pi == 0
          r[0].x = offsetX * -1
          r[0].allowedToMoveX = false
          r[0].image = 'img/graph/myself.svg'
          target =  r[r.length - 1]
          target.x = offsetX
          target.allowedToMoveX = false
          target.image = "img/graph/targeted#{_.capitalize target.knownType}.svg"
        paths.push r...

      nodes = []
      for p in paths
        if !_.find(nodes, (f) -> f.id == p.id)
          nodes.push(p)

      edges = []
      for singlePath in res
        for rp, i in singlePath
          if singlePath[i + 1]
              fromId = singlePath[i].id
              toId = singlePath[i + 1].id
              edges.push from : fromId, to : toId, color : "grey", style: 'dash-line'

      options =
        stabilize: false
        physics:
          barnesHut:
            gravitationalConstant: -200000,
            centralGravity: 3,
            springLength: 0,
            springConstant: 0.001,
            damping: 0.5
        smoothCurves: false

      data =
        nodes: nodes
        edges: edges

      network = new vis.Network(ele[0], data, options)
      network.zoomExtent()