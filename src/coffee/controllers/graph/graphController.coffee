"use strict"

app.factory "graphDataProvider", ($resource, $q, webApiConfig) ->

  resource = $resource(webApiConfig.url + "graph/:type/:id", {id : "@id", type : "@type"})

  map = (graph) ->

    nodes = _.flatten graph.map (m) -> [{attrs : m.fromNode}, {attrs : m.toNode}]
    nodes = _.uniq nodes, (n) -> n.attrs.id

    links = graph.map (m) ->

      attrs :
        id : m.id
        label : m.label
      source : nodes.filter((f) -> m.fromNode.id == f.attrs.id)[0]
      target : nodes.filter((f) -> m.toNode.id == f.attrs.id)[0]

    nodes : nodes
    links : links

  load : (node, step) ->
    deferred = $q.defer()
    resourceType = _.pluralize(node.type)
    resource.query {id : node.id, type : resourceType, step : step}, (graph_data) ->
      data = map graph_data
      deferred.resolve data
    , (err) ->
      deferred.reject err
    deferred.promise


app.controller "graphController", ($scope, graphDataProvider, $rootScope, $location) ->

  $scope.graph = null
  $scope.step = 1

  $rootScope.switchPage = ->
    $location.path "/panel"

  $scope.loadGraph = (node, step) ->
    graphDataProvider.load(node, step).then (data) ->
      $scope.graph = data

  ###
  $scope.$watch "user", (val) ->
    if val
      console.log "user changed", val
      $scope.loadGraph(type : "person")
  , true
  ###

  $scope.$watch "step", (val) ->
    console.log "step changed", val
    $scope.loadGraph({type : "person"}, val)