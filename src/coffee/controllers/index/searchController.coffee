"use strict"

app.controller "searchController", ($scope, itemsListService, $rootScope, storage) ->

  $scope.search = result : null, term : null

  search = (filter) ->
    itemsListService.pushSearchItem(filter).then (res) ->
      $scope.filter.subjectType = filter.type
      $scope.search.result = res.subject
      storage.put "filter", filter

  $scope.search = (filter, type) ->
    search term : filter, type : type, includeLinkedTags: true


  $rootScope.$on "user.changed", (evt, opts) ->
    onUserChanged()

  onUserChanged = (opts) ->
    console.log ">>>searchController.coffee:17"
    ###
    if !opts.user
      $scope.search.result = null
      #TODO : it should be invoked from "user.changed"
    else
      ###
    filter = storage.get "filter"
    if !filter
      filter = term : null, type : "person"
      ###
      if !itemsListService.items.length
        # if there is no items on the meadow, load latest filter
        search filter, true
      else
        #else just change filter type to show latest meadow item
    ###
    filter = storage.get "filter"
    if filter
      $scope.filter.subjectType = filter.type

  onUserChanged user : true

  $scope.termInputFocused = (f) ->
    setTimeout (-> if f then document.getElementById("term_input").select()), 100
