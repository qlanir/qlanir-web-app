"use strict"

app.controller "rootController", ($scope, $location, $rootScope, itemsListService,
                                  orgModal, personModal, offerModal, tagsModal) ->

  $scope.filter =
    user : null
    subjectType: "person"

  $rootScope.showProfile = ->
    itemsListService.showProfile()

  $rootScope.switchPage = ->
    $location.path "/graph"

  $rootScope.newOrg = ->
    orgModal.open()

  $rootScope.newPerson = ->
    personModal.open()

  $rootScope.newOffer = ->
    offerModal.open()

  $rootScope.editOffer = (item)->
    offerModal.open(item : item)

  $rootScope.editOrg = (item)->
    orgModal.open(item : item)

  $rootScope.editPerson = (item)->
    personModal.open(item : item)

  $rootScope.editTags = ->
    tagsModal.open()

  $rootScope.resetUI = ->
    itemsListService.swap()
