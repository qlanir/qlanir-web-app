"use strict"

app.controller "meadowController", ($scope, itemsListService, $rootScope, confirmDeleteModal, pathsDisplayModal, utils) ->

  $scope.deleteLink = (item, link) ->
    _item = item : link;
    _item.item.type = "link";
    confirmDeleteModal.open(_item).then ->
      itemsListService.removeLink(item.subject, link)

  $scope.displayPaths = (item) ->
    pathsDisplayModal.open(item.id)

  $scope.items = itemsListService.items

  $scope.openItem = (issuerItem, subj, evt) ->
    if !evt || (evt.target.className != "operations" && evt.target.parentElement.className != "operations")
      itemsListService.pushSubjectItem issuerItem, subj

  $scope.closeItem = itemsListService.removeItem

  $scope.pinItem = itemsListService.pinItem

  $scope.isSelected = itemsListService.isSelected

  $scope.isFilterHit = (issuerItem, subj) ->
    utils.isFilterHit itemsListService.items, issuerItem, subj

  $scope.isContainerFiltered = (issuerItem) ->
    utils.isContainerFiltered itemsListService.items, issuerItem

  $scope.removeItem = (item) ->
    confirmDeleteModal.open(item : item).then ->
      itemsListService.removeItem item

  $scope.updatePersonLinkRole = (item, linkItem, data) ->
    console.log "updatePersonLinkRole", data

  $scope.newMailBox = itemsListService.regenMailbox

  $scope.isMailBox = (contact) ->
    return (contact.subject.contactType.id == -603)

  $scope.canBeConverted = (item) ->
    utils.canBeConverted item

  $scope.convert = itemsListService.convertSubject
  $scope.confirm = itemsListService.confirmSubject


