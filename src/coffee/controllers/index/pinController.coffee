"use strict"

app.controller "pinController", ($scope, pinService, confirmDeleteModal) ->

  $scope.pins = pinService.pins

  $scope.loadPin = pinService.loadPin

  $scope.unpin = (pin)->
    confirmDeleteModal.open(item:{type:'pin'}).then ->
      pinService.unpin(pin)

  $scope.updatePin = pinService.updatePin