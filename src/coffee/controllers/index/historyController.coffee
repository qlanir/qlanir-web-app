"use strict"

app.controller "historyController", ($scope, historyService, typeNameFilter) ->

  $scope.stack = historyService.stack

  $scope.restorePoint = (point) ->
    historyService.restorePoint point
    if point.items[0].type == "search-results"
      $scope.filter.subjectType = point.items[0].subjectType

  $scope.getCaption = (i) ->
    item = i.item
    if item.type == "search-results"
      res = typeNameFilter(item.subjectType)
      if item.name
        res += " - " + item.name
    else
      res = item.name
    res


