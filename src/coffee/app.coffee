"use strict"

app = angular.module("qlanir-app", [
  "ngResource","ngRoute", "ngSanitize",
  "toaster", "xeditable", "ui.bootstrap",
  "ngTagsInput", "baio-ng-cordova-auth",
  "angular-data.DSCacheFactory"
])

app.config ($routeProvider) ->
  $routeProvider
  .when '/panel',
    templateUrl: 'html/panel.html'
    controller: 'rootController'
  .when '/graph',
    templateUrl: 'html/graph.html'
    controller: 'graphController'
  .when '/logon',
      template: '<h1></h1>'
      controller: ($rootScope, auth, $location) ->
        auth.logon($location.$$search.token).then ->
          #if login success redirect to main page
          $location.url "/panel"
  .otherwise
    redirectTo: '/panel'

app.config ($httpProvider) ->
  $httpProvider.responseInterceptors.push "ajaxFailureInterceptor"

angular.element(document).ready ->
  angular.bootstrap(document, ["qlanir-app"])

app.config (authProvider, authApiConfig) ->
  authProvider.setUrl authApiConfig.url

app.run (auth, $rootScope, itemsListService, adminService, appConfig) ->

  if appConfig.env.indexOf("test") != -1
    return

  console.log """
    ********************************************************
    ***               WELLCOME TO QLANIR                 ***
    ***                                                  ***
    ********************************************************
    App version : #{appConfig.version}
    App enviroment : #{appConfig.env}
  """
  adminService.getVersion().then (version) ->
    console.log """
    Api version : #{version.major}.#{version.minor}.#{version.minorRevision}
    """

  $rootScope.$on "user.changed", (evt, opts) ->
    if opts.user
      console.log ">>>app.coffee:56", "TODO : uncomment"
      itemsListService.restorePins()
      itemsListService.restoreHistory()
    else
      itemsListService.swap()
      $scope.search = null

  $rootScope.$on '$routeChangeStart', (evt, route) ->

    if route.$$route and route.$$route.originalPath != "/logon"
      #Any path should be opened only after login success
      #(whereas /logon shouldn't be authorized, since authorization will relay on this route to extract token and endeed meke authorization)
      #Just try to login, `login` directive will handle the rest

      auth.login()

app.config (DSCacheFactoryProvider) ->
  DSCacheFactoryProvider.setCacheDefaults
    maxAge: 1000 * 60 * 60 * 24
    deleteOnExpire: 'aggressive'
    storageMode: 'localStorage'
