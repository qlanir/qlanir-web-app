"use strict"

app.factory "adminService", ($resource, $q, webApiConfig) ->

  resource = $resource(webApiConfig.url + ":act", act: "@act")

  getVersion : ->

    def = $q.defer()

    resource.get(
      act : "version",
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  ###
  action : (name) ->

    def = $q.defer()

    resource.query(
      act : name,
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )

    def.promise
  ###