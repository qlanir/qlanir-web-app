"use strict"

app.factory "utils", ->

  isFilterHit: (items, issuerItem, subj) ->
    index = items.indexOf issuerItem
    searchItem = items[index-1]

    if searchItem && searchItem.type == "search-results"
      hits = searchItem.selectedSubject.hits

      if !hits
        selectedItemWithHits = searchItem.subject.results.filter((f) -> f.id == searchItem.selectedSubject.id)[0]
        if selectedItemWithHits
          hits = selectedItemWithHits.hits
        else
          return false

      for hit in hits
        _hit = hit.hit.replace(/<em>/g,'').replace /<\/em>/g,''
        if hit.type=="offer" && subj.subject.label == _hit
          return true
        else if hit.type=="person"
          if (subj.hostSubject && subj.hostSubject.type=="person" && subj.hostSubject.label == _hit) || (hit.type=="person" && subj.proxy && subj.proxy.type=="person" && subj.proxy.label == _hit)
            return true
        else if hit.type=="organization" && subj.hostSubject.type=="organization" && subj.hostSubject.label == _hit
          return true
    false

  isContainerFiltered: (items, issuerItem) ->
    index = items.indexOf issuerItem
    searchItem = items[index-1]
    if searchItem && searchItem.type == "search-results" && searchItem.name
      return true
    false

  canBeConverted: (item) ->
    subject = item.subject
    if subject.people.length == 0 && subject.organizations.length == 0 && subject.offers.length == 0
      if subject.tags.filter((f) -> f.id == -601)[0] && subject.tags.filter((f) -> f.id == -602)[0]
        return true
    false
