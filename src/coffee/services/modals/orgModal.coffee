"use strict"

orgModalInstanceController = ($scope, $modalInstance, itemsService, searchService) ->


  item2modalJson = (data) ->
    res = {}
    res.orgId = data.id
    res.orgName = data.label
    res.contacts = data.contacts.map (m) ->
      id : m.id
      contactType : $scope.sets.contactTypes.filter((f) -> f.id == m.subject.contactType.id)[0] # {id: m.subject.contactType.id, name : m.subject.contactType.label}
      contactValue : m.subject.label
    res.people = data.people.map (m) ->
      id : m.id
      personId : m.subject.id
      personRole : $scope.sets.personRoles.filter((f) -> f.id == m.tag.id)[0] # {id : m.tag.id, name : m.tag.label}
      personName : m.subject.label
    res.organizations = []
    res.offers = data.offers.map (m) ->
      id : m.id, offerName : m.tag.label, offerDescription: m.subject.label, proxyId: m.proxy?.id
    res.tags = data.tags

    if res.contacts.length == 0
      res.contacts = [
        id : null
        contactType : $scope.sets.contactTypes[0]
        contactValue : null
      ]

    if res.people.length == 0
      res.people = [
        id : null
        personName : null
        personRole : $scope.sets.personRoles[0]
      ]

    if res.offers.length == 0
      res.offers = [
        id : null
        offerName : null
      ]

    res

  toServiceJson = (data) ->
    res =
      id : data.orgId
      label : data.orgName
      contacts : data.contacts.map((m) -> id : m.id, subject : {contactType : m.contactType, label : m.contactValue}).filter (f) -> f.subject.label
      people : data.people.map((m) -> id : m.id, tag : {id : m.personRole.id}, subject : {id : m.personId, label : m.personName}).filter (f) -> f.subject.label
      organizations : []
      offers : data.offers.map((m) -> id : m.id, tag : {label : m.offerName}, subject : {id : m.id, label: m.offerDescription}, proxy : {id: m.proxyId}).filter (f) -> f.tag.label
      tags : data.tags

    for person in res.people
      personRoleId = person.tag.id
      mutual = $scope.sets.mutualRelTypes.filter((f) -> f.indexOf(personRoleId) != -1)[0]
      if mutual
        person.hostTag = id : mutual.filter((f) -> f != personRoleId)[0]

    res

  $scope.sets =
    contactTypes : [
      { id : -12, name : "телефон" }
      { id : -13, name : "google contact" }
      { id : -14, name : "e-mail" }
      { id : -15, name : "skype" }
      { id : -16, name : "сайт" }
    ]
    personRoles : [
      { id : -31, name : "наёмник" }
      { id : -28, name : "владелец" }
      { id : -22, name : "учредитель" }
      { id : -16, name : "генеральный дир." }
    ]
    mutualRelTypes: [
      [-28, -29] #владелец -> во-владении
      [-22, -23] #учредитель -> учреждение
    ]

  defaultData = null

  if $modalInstance.rootData
    defaultData = item2modalJson $modalInstance.rootData
  else
    defaultData =
      orgName : null
      contacts : [
        id : null
        contactType : $scope.sets.contactTypes[0]
        contactValue : null
      ]
      offers : [
        id : null
        offerName : null
      ]
      people : [
        id : null
        personId : null
        personName : null
        personRole : $scope.sets.personRoles[0]
      ]
      tags : []

  $scope.data = {}

  reset = (setPrestine) ->

    $scope.data.orgId = defaultData.orgId
    $scope.data.orgName = defaultData.orgName
    $scope.data.contacts = defaultData.contacts.map (m) -> id : m.id, contactValue : m.contactValue, contactType : m.contactType
    $scope.data.offers = defaultData.offers.map (m) -> id : m.id, offerName : m.offerName, offerDescription: m.offerDescription, proxyId: m.proxyId
    $scope.data.people = defaultData.people.map (m) -> id : m.id, personId : m.personId, personName : m.personName, personRole : m.personRole
    $scope.data.tags = defaultData.tags.map (m) -> label : m.label

    if setPrestine
      $scope.data.orgModal.$setPristine()

  reset()

  $scope.saveAndExit =  ->
    #FG: See bug with form names here : https://github.com/angular-ui/bootstrap/issues/969
    if $scope.data.orgModal.$valid
      data = $scope.data
      serviceData = toServiceJson data
      $scope.isSaving = true
      itemsService.saveSubject(serviceData, "organization").then (res) ->
        $scope.isSaving = false
        $modalInstance.close res
      , (err) ->
        $scope.isSaving = false
    else
      alert "Not valid fields"

  $scope.cancelAndExit = ->
    $modalInstance.dismiss "cancel"

  $scope.reset = -> reset(true)

  $scope.addContact = ->
    $scope.data.contacts.push
      contactType : $scope.sets.contactTypes[0]
      contactValue : null

  $scope.addPerson = ->
    $scope.data.people.push
      personRole : $scope.sets.personRoles[0]
      personName : null

  $scope.addOffer = ->
    $scope.data.offers.push
      offerName : null

  $scope.setOfferType = (offer, item) ->
    offer.offerName = item.label
    #service.serviceTypeId = item.id

  $scope.setPerson = ($model, $item) ->
    $model.personName = $item.label
    $model.personId = $item.id

  $scope.addTag = (tag) ->

  $scope.removeTag = (tag) ->

  $scope.loadTags = (term) ->
    searchService.searchNames(term : term, type : "tag")

  $scope.loadPeople = (term, $model) ->
    $model.personId = null
    searchService.searchNames(term : term, type : "person")

  $scope.isSaving = false

  $scope.popUpCss = ->
    if $modalInstance.rootData then "editOrganization" else "addOrganization"

  $scope.contactCss = (contact) ->
    if !contact.contactType
      return ""
    switch contact.contactType.id
      when -12 then return "mobPhone"
      when -18, -13 then return "google"
      when -14 then return "mail"
      when -15 then return "skype"
      when -16 then return "site"
      when -17 then return "facebook"
      when -19 then return "twitter"
      when -20 then return "vkontakte"
      else return ""

  $scope.resetText = -> if $modalInstance.rootData then "Вернуть" else "Очистить"


app.factory "orgModal", ($modal) ->

  open: (modalOptions) ->

    opts =
      templateUrl: "modals/org.html"
      controller: orgModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalOptions = angular.extend {}, opts, modalOptions

    modalInstance = $modal.open modalOptions
    if modalOptions?.item
      modalInstance.rootData = modalOptions.item.subject

    modalInstance.result.then ((data) ->
      console.log "Modal result", data
    ), ->
      console.log "Modal dismissed"
    modalInstance







