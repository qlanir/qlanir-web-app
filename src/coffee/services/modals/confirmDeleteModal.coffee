"use strict"

confirmDeleteModalInstanceController = ($scope, $modalInstance) ->

  $modalInstance.item

  if $modalInstance.item?.type == "link" && $modalInstance.item.subject.type != "offer"
    $scope.dataType = "связь"
    if $modalInstance.item.subject.type == "person"
      $scope.dataType = $scope.dataType.concat " с персоной"
    else if $modalInstance.item.subject.type == "organization"
      $scope.dataType = $scope.dataType.concat " с организацией"
    else if $modalInstance.item.subject.type == "contact"
      $scope.dataType = "контактные данные"
  else if $modalInstance.item?.type == "pin"
    #$scope.message = "Пояснение для прикрепленной записи удалится навсегда."
    $scope.dataType = "прикрепление с комментарием"
  else if $modalInstance.item?.type == "person"
    $scope.dataType = "персону"
  else if $modalInstance.item?.type == "organization"
    $scope.dataType = "организацию"
  else if $modalInstance.item?.type == "offer" || ($modalInstance.item?.type == "link" && $modalInstance.item.subject.type == "offer")
    $scope.dataType = "предложение услуги"
  else
    $scope.dataType = "запись"

  $scope.confirmDelete =  ->
    $modalInstance.close true

  $scope.cancelDelete =  ->
    $modalInstance.dismiss false

app.factory "confirmDeleteModal", ($modal) ->

  open: (modalOptions) ->

    opts =
      templateUrl: "modals/confirmDelete.html"
      controller: confirmDeleteModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalOptions = angular.extend {}, opts, modalOptions

    modalInstance = $modal.open modalOptions
    if modalOptions?.item
      modalInstance.item = modalOptions.item

    modalInstance.result






