"use strict"

personModalInstanceController = ($scope, $modalInstance, itemsService, searchService) ->

  item2modalJson = (data) ->
    res = {}
    res.personId = data.id
    res.personDate = data.birthdate
    res.sex =  if data.sex != null then $scope.sets.sexTypes[data.sex] else "unknown"
    res.name = angular.copy data.nameParts
    res.name.fullName = data.label
    res.contacts = data.contacts.map (m) ->
      id : m.id
      contactType : $scope.sets.contactTypes.filter((f) -> f.id == m.subject.contactType.id)[0] # {id: m.subject.contactType.id, name : m.subject.contactType.label}
      contactValue : m.subject.label
    res.people = data.people.map (m) ->
      id : m.id
      personId : m.subject.id
      hostRole : $scope.sets.relTypes.filter((f) -> f.id == m.hostTag.id)[0]
      personRole : $scope.sets.relTypes.filter((f) -> f.id == m.tag.id)[0]
      personName : m.subject.label
    res.orgs = data.organizations.map (m) ->
      id : m.id, orgId : m.subject.id, orgName : m.subject.label
    res.offers = data.offers.map (m) ->
      id : m.id, offerName : m.tag.label, offerDescription: m.subject.label, proxyId: m.proxy?.id
    res.tags = data.tags
    ###if data.people.length == 1
      if data.people[0].proxy
        res.relationType = "proxy"
        res.relationProxyLabel = data.people[0].proxy.label
      else
        res.relationType = "direct"
        res.personRelType = $scope.sets.relTypes.filter( (f) -> f.id == data.people[0].tag.id )[0]
        res.ownerRelType = $scope.sets.relTypes.filter( (f) -> f.id == data.people[0].hostTag.id )[0]
    else###
    res.relationType = "none"
    res.relationProxyLabel = null
    res.personRelType = $scope.sets.relTypes[0]
    res.ownerRelType = $scope.sets.relTypes[0]

    if res.people.length == 0
      res.people = [
        id : null
        personName : null
        personRole : $scope.sets.relTypes[0]
        hostRole : $scope.sets.relTypes[0]
      ]

    if res.contacts.length == 0
      res.contacts = [
        id : null
        contactType : $scope.sets.contactTypes[0]
        contactValue : null
      ]

    if res.orgs.length == 0
      res.orgs = [
        id : null
        orgName : null
      ]

    if res.offers.length == 0
      res.offers = [
        id : null
        offerName : null
      ]

    res

  toServiceJson = (data) ->
    console.log data

    for person in data.people
      personRoleId = person.personRole.id
      mutual = $scope.sets.mutualRelTypes.filter((f) -> f.indexOf(person.personRole.id) != -1)[0]
      if mutual
        person.hostRole = id : mutual.filter((f) -> f != person.personRole.id)[0]
      else
        person.hostRole = id : person.personRole.id

    res =
      id : data.personId
      label : data.name.fullName
      nameParts : angular.copy data.name
      birthDate : data.personDate
      sex : if data.sex == "male" then 1 else (if data.sex == "female" then 0 else null)
      contacts : data.contacts.map((m) -> id : m.id, subject : {contactType : m.contactType, label : m.contactValue}).filter (f) -> f.subject.label && f.subject.contactType #(&& f.subject.contactType)временно, для профиля с личным mailbox
      people : data.people.map((m) -> id : m.id, tag : {id : m.personRole.id}, hostTag : {id : m.hostRole.id}, subject : {id : m.personId, label : m.personName}).filter (f) -> f.subject.label
      organizations : data.orgs.map((m) -> id : m.id, subject : {label : m.orgName, id: m.orgId}).filter (f) -> f.subject.label
      offers : data.offers.map((m) -> id: m.id, tag : {label : m.offerName}, subject : {id: m.id, label: m.offerDescription}, proxy : {id: m.proxyId}).filter (f) -> f.tag.label
      tags : data.tags.map((m) -> label : m.label).filter (f) -> f.label
    delete res.nameParts.fullName
    if data.relationType != "none"
      #link = tag : {label : data.personRelType.name}, hostTag : {label : data.ownerRelType.name}
      link = hostTag : {id : data.personRelType.id}
      mutual = $scope.sets.mutualRelTypes.filter((f) -> f.indexOf(data.personRelType.id) != -1)[0]
      if mutual
        link.tag = id : mutual.filter((f) -> f != data.personRelType.id)[0]
      else
        link.tag = id : data.personRelType.id

      res.people.push link
      if data.relationType == "proxy"
        link.proxy = {label : data.relationProxyLabel, id: data.relationProxyId}
    res

  $scope.sets =
    contactTypes : [
      { id : -12, name : "телефон" }
      { id : -18, name : "google" }
      { id : -14, name : "e-mail" }
      { id : -15, name : "skype" }
      { id : -16, name : "сайт" }
      { id : -17, name : "facebook"}
      { id : -19, name : "twitter"}
      { id : -20, name : "vkontakte"}
    ]
    relTypes : [
      { id : -39, name : "знакомый" }
      { id : -158, name : "друг" }
      { id : -159, name : "муж" }
      { id : -160, name : "жена" }
      { id : -152, name : "родственник" }
      { id : -153, name : "родитель" }
      { id : -154, name : "ребенок" }
      { id : -5, name : "партнер" }
      { id : -157, name : "неприятель" }
    ]
    mutualRelTypes: [
      [-153, -154]
      [-159, -160]
    ]
    sexTypes: [
      "female"
      "male"
    ]

  defaultData = null

  if $modalInstance.rootData
    defaultData = item2modalJson $modalInstance.rootData
  else
    defaultData =
      name :
        fullName : null
        firstName : null
        lastName : null
        middleName : null
        nickName : null
      personDate : null
      sex : "unknown"
      contacts : [
        contactType : $scope.sets.contactTypes[0]
        contactValue : null
      ]
      people : [
        id : null
        personId : null
        personName : null
        personRole : $scope.sets.relTypes[0]
        hostRole : $scope.sets.relTypes[0]
      ]
      offers : [
        offerName : null
      ]
      orgs : [
        id : null
        orgId : null
        orgName : null
      ]
      tags : []
      relationType : "none"
      relationProxyLabel : null
      personRelType : $scope.sets.relTypes[0]
      ownerRelType : $scope.sets.relTypes[0]

  #reset data work only this way, since we need ref to *Types fields for `select`, not copies
  reset = (setPrestine) ->
    $scope.data.personId = defaultData.personId
    $scope.data.personDate = defaultData.personDate
    $scope.data.sex = defaultData.sex
    $scope.data.name = angular.copy defaultData.name
    $scope.data.people = defaultData.people.map (m) -> id : m.id, personId : m.personId, personName : m.personName, personRole : m.personRole, hostRole : m.hostRole
    $scope.data.contacts = defaultData.contacts.map (m) -> id : m.id, contactValue : m.contactValue, contactType : m.contactType
    $scope.data.offers = defaultData.offers.map (m) -> id : m.id, offerName : m.offerName, offerDescription: m.offerDescription, proxyId: m.proxyId
    $scope.data.orgs = defaultData.orgs.map (m) -> id : m.id, orgId : m.orgId, orgName : m.orgName
    $scope.data.tags = defaultData.tags.map (m) -> label : m.label
    $scope.data.relationType = defaultData.relationType
    $scope.data.relationProxyLabel = defaultData.relationProxyLabel
    $scope.data.personRelType = defaultData.personRelType
    $scope.data.ownerRelType = defaultData.ownerRelType
    $scope.showPersonNameCard = false

    if setPrestine
      $scope.data.personModal.$setPristine()

  $scope.data = {}
  reset()

  $scope.switchRelationDirect = ->
    if $scope.data.relationType == "direct"
      $scope.data.relationType = "none"
    else
      $scope.data.relationType = "direct"

  $scope.switchRelationProxy = ->
    if $scope.data.relationType == "proxy"
      $scope.data.relationType = "none"
    else
      $scope.data.relationType = "proxy"

  $scope.switchMaleSex = ->
    if $scope.data.sex == "female" || $scope.data.sex == "unknown"
      $scope.data.sex = "male"
    else
      $scope.data.sex = "unknown"
      $scope.data.personModal.$pristine = false #bug with personModal.$pristine, workaround

  $scope.switchFemaleSex = ->
    if $scope.data.sex == "male" || $scope.data.sex == "unknown"
      $scope.data.sex = "female"
    else
      $scope.data.sex = "unknown"
      $scope.data.personModal.$pristine = false #bug with personModal.$pristine, workaround

  $scope.saveAndExit =  ->
    #FG: See bug with form names here : https://github.com/angular-ui/bootstrap/issues/969
    if $scope.data.personModal.$valid
      data = $scope.data
      serviceData = toServiceJson data
      $scope.isSaving = true
      console.log ">>>personModal.coffee:232"
      itemsService.saveSubject(serviceData, "person").then (res) ->
        $scope.isSaving = false
        $modalInstance.close res
      , (err) ->
        $scope.isSaving = false
    else
      alert "Not valid fields"

  $scope.cancelAndExit = ->
    $modalInstance.dismiss "cancel"

  $scope.reset = -> reset true

  $scope.addContact = ->
    $scope.data.contacts.push
      contactType : $scope.sets.contactTypes[0]
      contactValue : null

  $scope.addOrg = ->
    $scope.data.orgs.push
      orgName : null

  $scope.addPerson = ->
    $scope.data.people.push
      personName : null
      personId : null
      personRole : $scope.sets.relTypes[0]
      hostRole : $scope.sets.relTypes[0]

  $scope.addOffer = ->
    $scope.data.offers.push
      offerName : null

  $scope.addTag = (tag) ->

  $scope.removeTag = (tag) ->

  $scope.setOfferType = (offer, item) ->
    offer.offerName = item.label
    #service.serviceTypeId = item.id

  $scope.setOrg = (org, item) ->
    org.orgName = item.label
    org.orgId = item.id

  $scope.setPerson = (person, item) ->
    console.log "setPerson"
    person.personName = item.label
    person.personId = item.id

  $scope.setPersonPerformer = (item) ->
    $scope.data.relationProxyLabel = item.label
    $scope.data.relationProxyId = item.id

  $scope.loadTags = (term) ->
    searchService.searchNames(term : term, type : "tag")

  $scope.loadOrgs = (term, org) ->
    org.orgId = null
    searchService.searchNames(term : term, type : "organization")

  $scope.loadPeople = (person, term) ->
    person.personId = null
    searchService.searchNames(term : term, type : "person")

  $scope.loadPerformers = (term) ->
    $scope.data.relationProxyId = null
    searchService.searchNames(term : term, type : "person")

  $scope.isSaving = false

  $scope.popUpCss = ->
    if $modalInstance.rootData then "editPerson" else "addPerson"

  $scope.contactCss = (contact) ->
    if !contact.contactType
      return ""
    switch contact.contactType.id
      when -12 then return "mobPhone"
      when -18, -13 then return "google"
      when -14 then return "mail"
      when -15 then return "skype"
      when -16 then return "site"
      when -17 then return "facebook"
      when -19 then return "twitter"
      when -20 then return "vkontakte"
      else return ""

  $scope.inputFullNameChanged = ->
    val = $scope.data.name.fullName
    if val
      spts = val.split " "
      #console.log val, spts, spts.length
      $scope.data.name.lastName = if spts.length > 1 then spts[0] else null
      $scope.data.name.firstName = if spts.length == 1 then spts[0] else spts[1]
      $scope.data.name.middleName = spts[2..].join(" ")
    else
      $scope.data.name.firstName = null
      $scope.data.name.lastName = null
      $scope.data.name.middleName = null

  composeFullName = ->
    res = []
    if $scope.data.name.lastName then res.push $scope.data.name.lastName
    if $scope.data.name.firstName then res.push $scope.data.name.firstName
    if $scope.data.name.middleName then res.push $scope.data.name.middleName
    fullName = res.join(" ")
    $scope.data.name.fullName = fullName
    #console.log fullName

  $scope.$watch "data.name.firstName", ->
    composeFullName()

  $scope.$watch "data.name.lastName", ->
    composeFullName()

  $scope.$watch "data.name.middleName", ->
    composeFullName()

  $scope.showPersonNameCard = false
  $scope.switchPersonNameCard = -> $scope.showPersonNameCard = !$scope.showPersonNameCard

  $scope.resetText = -> if $modalInstance.rootData then "Вернуть" else "Очистить"


app.factory "personModal", ($modal) ->

  controller : personModalInstanceController

  open: (modalOptions) ->

    opts =
      templateUrl: "modals/person.html"
      controller: personModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalOptions = angular.extend {}, opts, modalOptions

    modalInstance = $modal.open modalOptions
    if modalOptions?.item
      modalInstance.rootData = modalOptions.item.subject

    modalInstance.result.then ((data) ->
      ###
      if modalOptions.item
        angular.copy data, modalOptions.item.subject
      ###
      console.log "Modal result", data
    ), ->
      console.log "Modal dismissed"
    modalInstance






