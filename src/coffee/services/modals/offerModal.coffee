"use strict"

offerModalInstanceController = ($scope, $modalInstance, itemsService, searchService) ->

  toServiceJson = (data) ->
    #TODO : все же лучше отправлять на сервер только лэйблы без айдишников! и на сервере потом можно будет искать по лейблам
    res =
      id : $scope.data.id
      description : data.offerDescription
      type : data.offerType

    if data.isProxy
      res.proxy = data.hostSubject
      if data.proxySubject && data.proxySubject.label
        res.linkedSubject = data.proxySubject
    else
      res.linkedSubject = data.hostSubject

    if res.linkedSubject && !res.linkedSubject.type
      res.linkedSubject.type = "person"

    res


  #поменял имя, так как toClient предрологает что конвертим серверные данные, тут все же конвертим клиентские из одного формата в другой
  subject2modalJson = (data) ->
    res = {}
    res.id = data.id
    res.offerType = data.tags[0]
    res.offerDescription = data.label
    res.isProxy = true;
    res.hostSubject = {}
    res.proxySubject = {}

    host = data.people.filter((f) -> f.tag.id == -42)[0]
    if (!host)
      host = data.organizations.filter((f) -> f.tag.id == -42)[0]

    proxy = data.people.filter((f) -> f.tag.id == -43)[0]
    if (!proxy)
      proxy =
        label : null
        id : null
      res.isProxy = false

    if (res.isProxy)
      res.hostSubject.label = proxy.subject.label
      res.hostSubject.type = proxy.subject.type
      res.hostSubject.id = proxy.subject.id
      if host
        res.proxySubject.label = host.subject.label
        res.proxySubject.type = host.subject.type
        res.proxySubject.id = host.subject.id
    else
      res.hostSubject.label = host.subject.label
      res.hostSubject.type = host.subject.type
      res.hostSubject.id = host.subject.id

    res

  defaultData = null

  if $modalInstance.rootData
    defaultData = subject2modalJson $modalInstance.rootData
  else
    defaultData =
      id : null
      offerType :
        id : null
        label : null
      hostSubject :
        label : null
        id : null
        type: null
      proxySubject :
        label : null
        id : null
      offerDescription : null
      isProxy: false

  resetData = ->
    $scope.data.id = defaultData.id
    $scope.data.offerType = id : defaultData.offerType.id, label : defaultData.offerType.label
    $scope.data.hostSubject = id : defaultData.hostSubject.id, label : defaultData.hostSubject.label, type : defaultData.hostSubject.type
    $scope.data.proxySubject = id : defaultData.proxySubject.id, label : defaultData.proxySubject.label, type : defaultData.proxySubject.type
    $scope.data.offerDescription = defaultData.offerDescription
    $scope.data.isProxy = defaultData.isProxy

  $scope.data = {}
  resetData()

  $scope.setHostSubject = (item) ->
    $scope.data.hostSubject.label = item.label
    $scope.data.hostSubject.id = item.id
    $scope.data.hostSubject.type = item.type

  $scope.setProxySubject = (item) ->
    $scope.data.proxySubject.label = item.label
    $scope.data.proxySubject.id = item.id
    $scope.data.proxySubject.type = item.type

  $scope.setOfferType = (item) ->
    $scope.data.offerType.label = item.label
    $scope.data.offerType.id = item.id

  $scope.switchToPersonPerformer = ->
    $scope.data.hostSubject.type = "person"

  $scope.switchToOrgPerformer = ->
    $scope.data.hostSubject.type = "organization"

  $scope.saveAndExit =  ->
    if $scope.data.offerModal.$valid
      data = $scope.data
      serviceData = toServiceJson data
      $scope.isSaving = true
      itemsService.saveSubject(serviceData, "offer").then (res) ->
        $scope.isSaving = false
        $modalInstance.close res
      , (err) ->
        $scope.isSaving = false
    else
      alert "Not valid fields"

  $scope.cancelAndExit = ->
    $modalInstance.dismiss "cancel"

  $scope.reset = ->
    resetData()
    $scope.data.offerModal.$setPristine()

  $scope.loadTags = (term) ->
    searchService.searchNames(term : term, type : "tag")

  $scope.loadPeople = (term, subject) ->
    subject.id = null
    searchService.searchNames(term : term, type : "person")

  $scope.loadPeopleAndOrgs = (term, subject) ->
    subject.id = null
    subject.type = null
    searchService.searchNames(term : term, type : "organization&person")

  $scope.isSaving = false

  $scope.popUpCss = ->
    if $modalInstance.rootData then "editOffer" else "addOffer"

  $scope.resetText = -> if $modalInstance.rootData then "Вернуть" else "Очистить"

app.factory "offerModal", ($modal) ->


  open: (modalOptions) ->

    opts =
      templateUrl: "modals/offer.html"
      controller: offerModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalOptions = angular.extend {}, opts, modalOptions

    modalInstance = $modal.open modalOptions
    if modalOptions?.item
      modalInstance.rootData = modalOptions.item.subject

    modalInstance.result.then ((data) ->
      console.log "Modal result", data
    ), ->
      console.log "Modal dismissed"
    modalInstance







