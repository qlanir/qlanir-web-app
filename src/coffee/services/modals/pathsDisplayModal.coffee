"use strict"

pathsDisplayModalInstanceController = ($scope, $modalInstance, graphService) ->

  $scope.paths = graphService.paths($modalInstance.subjectId).then (res) ->
    $scope.noPaths = if res.length then false else true
    res

  $scope.cancel =  ->
    $modalInstance.dismiss false

app.factory "pathsDisplayModal", ($modal) ->

  open: (subjectId) ->

    opts =
      templateUrl: "modals/pathsDisplay.html"
      controller: pathsDisplayModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalInstance = $modal.open opts
    modalInstance.subjectId = subjectId

    modalInstance.result






