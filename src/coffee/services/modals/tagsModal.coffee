"use strict"

tagsModalInstanceController = ($scope, $modalInstance, searchService, tagsService) ->

  $scope.tags = []
  $scope.inputTag = null
  $scope.exit = ->
    $modalInstance.dismiss "cancel"

  $scope.inputTagChanged = (val) ->
    if val
      searchService.searchNames(term : val, type : "tag").then (res) ->
        $scope.tags = res.map (m) -> id : m.id, name : m.label
    else
      $scope.tags = []

  $scope.createTag = (label) ->
    $scope.isSaving = true
    tagsService.createTag(label).then (res) ->
      $scope.tags.splice 0, 0, id : res.id, name : res.label
      $scope.isSaving = false
    , ->
      $scope.isSaving = false

  $scope.askRemoveTag = (tag) ->
    console.log "ask remove tag", tag
    $scope.removingTag = tag
    tagsService.getTagLinksCount(tag.id).then (res) ->
      $scope.removingTag.linksCount = parseInt res["0"]

  $scope.removeAskingTag = ->
    console.log "remove tag"
    if $scope.removingTag
      $scope.isSaving = true
      tagsService.removeTag($scope.removingTag.id).then ->
        $scope.tags.splice $scope.tags.indexOf($scope.removingTag), 1
        $scope.removingTag = null
        $scope.isSaving = false
      , ->
        $scope.isSaving = false

  $scope.cancelAskingRemoveTag = ->
    $scope.removingTag = null

  $scope.isSaving = false
  $scope.removingTag = null

app.factory "tagsModal", ($modal) ->

  open: (modalOptions) ->

    opts =
      templateUrl: "modals/tags.html"
      controller: tagsModalInstanceController
      size: "lg"
      windowClass: "popUpScreen"

    modalOptions = angular.extend {}, opts, modalOptions

    modalInstance = $modal.open modalOptions

    modalInstance.result.then ((data) ->
      console.log "Modal result", data
    ), ->
      console.log "Modal dismissed"
    modalInstance







