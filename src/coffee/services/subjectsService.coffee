"use strict"

app.factory "subjectsService", ($resource, $q, webApiConfig, mapper) ->

  resource = $resource(webApiConfig.url + ":ref/:id", {id: "@id", ref: "@ref"}, {update : {method : 'PUT'}})
  linkResource = $resource(webApiConfig.url + ":ref/:id/:linkRef/:linkId", id: "@id", ref: "@ref", linkRef: "@linkRef", linkId: "@linkId")

  removeSubject : (subjectId, subjectType) ->
    def = $q.defer()
    resorceRef = _.pluralize subjectType
    resource.delete(
      id : subjectId,
      ref : resorceRef
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  getSubject : (subjectId, subjectType) ->
    def = $q.defer()
    resorceRef = _.pluralize subjectType
    resource.get(
      id : subjectId,
      ref : resorceRef
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  #POST request
  saveSubject : (subjectId, subjectType, data) ->
    def = $q.defer()
    resorceRef = _.pluralize subjectType
    _resource = if subjectId == 0 or subjectId then resource.update else resource.save
    _resource(
      { id : subjectId, ref : resorceRef },
      data,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  updateLink : (subjectId, subjectType, linkSubjectId, linkSubjectType, data) ->
    def = $q.defer()
    resorceRef = _.pluralize subjectType
    linkResorceRef = _.pluralize linkSubjectType
    linkResource.save(
      { id : subjectId, ref : resorceRef, linkId : linkSubjectId, linkRef : linkResorceRef },
      data,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  removeLink : (subjectId, subjectType, linkSubjectId, linkSubjectType) ->
    def = $q.defer()
    resorceRef = _.pluralize subjectType
    linkResorceRef = _.pluralize linkSubjectType
    linkResource.delete(
      { id : subjectId, ref : resorceRef, linkId : linkSubjectId, linkRef : "#{linkResorceRef}Links" },
      {},
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  regenMailbox : ->
    def = $q.defer()

    resource.save(
      { id : "", ref : "regenMailbox" },
      null,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise
