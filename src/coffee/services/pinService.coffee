"use strict"

app.factory "pinService", (pinsService, pinDescription, cache, $q) ->

  pins : []

  swapPins: ->
    @pins.splice 0, @pins.length

  restorePins: (onLoadPin) ->
    pinsService.getPins().then (pins) =>
      @pins.splice 0, @pins.length
      for pin in pins
        @pins.push item : pin, onLoadPin : onLoadPin
        cache.addPin pin
    , (err) ->
      console.log ">>>pinService.coffee:16", err

  unpin : (pin) ->
    @pins.splice @pins.indexOf(pin), 1
    pinsService.removePin(pin.item.id).then ->
      cache.removePin pin.item.id
    , (err) ->
      console.log ">>>pinService.coffee:23", err

  removeBySubject : (subj) ->
    for pin in @pins.filter((f) -> f.item.subject.id == subj.id and f.item.subject.type == subj.type)
      @pins.splice @pins.indexOf(pin), 1

  pinItem: (prevItem, item, onLoadPin) ->
    pin = @pins.filter((f) -> f.item.id == item.id)[0]
    if !pin
      descr = pinDescription.compose prevItem, item
      samePin = @pins.filter((f) -> f.item.subject.id == descr.ref &&  f.item.description == descr.descr)[0]
      if !samePin
        pinsService.createPin(descr.ref, descr.descr).then (res) =>
          pin = item : res, onLoadPin : onLoadPin
          @pins.push pin
          cache.addPin pin.item
          pin : pin, created : true
      else
        console.log "Same pin already exists"
        $q.when(pin : samePin, created : false)

  updatePin: (id, descr) ->
    pinsService.updatePin(id, descr)

  loadPin: (pin) ->
    pin.onLoadPin pin.item
