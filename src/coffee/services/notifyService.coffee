"use strict"

app.factory "notifyService", (toaster) ->

  warning : (msg, title) ->
    toaster.pop('warning', title, msg)

  error : (msg, title) ->
    toaster.pop('error', title, msg)

  success : (msg, title) ->
    toaster.pop('success', title, msg)
