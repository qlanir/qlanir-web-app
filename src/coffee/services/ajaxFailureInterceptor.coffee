app.factory "ajaxFailureInterceptor", ($injector, notifyService, authApiConfig, $q) ->
  (promise) ->
    $http = $injector.get("$http")
    promise.then null, (err) ->
      if err.code == 400
        url = authApiConfig.url
        url += "?lang=ru"
        window.location = url
      else if err.status == 404
        console.log ">>>ajaxFailureInterceptor.coffee:10", "Object Not Found"
        $q.reject err
      else
        notifyService.error err.data.message, "Ошибка запроса"
    promise