"use strict"

app.factory "itemsListService", ($q, itemsService, historyService, pinService) ->

  items = []

  swapLinkedOffers = (item) ->
    if item.subject?.offers
      for offer in item.subject.offers
        for offerItem in items.filter((f) -> f.subject and f.subject.type == offer.subject.type and f.subject.id == offer.subject.id)
          pinService.removeBySubject offerItem.subject
          offerItem._isRemoved = true

  swapEmptyServices = ->
    for serviceItem in items.filter((f) -> f.type == "service")
      if serviceItem.subject.offers.length == 0
        serviceItem._isRemoved = true

  loadPinItem = (pin) ->
    itemsService.getSubjectItem(pin.subject).then (item) ->
      items.splice 0, items.length, item
      historyService.storePoint items, restorePoint

  restorePoint = (historyItems) ->
    itemsService.restoreHistoryItems(historyItems).then (res) ->
      items.splice 0, items.length
      items.push res...
      for item, i in items
        item.selectedSubject = items[i + 1]

  getInsertIndex = (issuer) ->
    if issuer then items.indexOf(issuer) else 0

  items : items

  pushSearchItem: (filter) ->
    itemsService.getSearchItem(filter).then (item) ->
      items.splice 0, items.length, item
      historyService.storePoint items, restorePoint
      item

  pushSubjectItem: (issuerItem, subj) ->
    promise = itemsService.getSubjectItem(subj)
    promise.then (item) ->
      if issuerItem
        issuerItem.selectedSubject = subj
        index = getInsertIndex(issuerItem)
      else
        index = -1
      items.splice index + 1, items.length,  item
      historyService.storePoint items, restorePoint
    promise

  appendSubjectItems: (items) ->
    lastItem = _.last(items)
    promise = @pushSubjectItem lastItem, items[0]
    promise

  isSelected: (issuerItem, subj) ->
    issuerItem.selectedSubject and issuerItem.selectedSubject.id == subj.id

  removeItem: (item) ->
    itemsService.removeItem(item).then ->
      _items = items.filter((f) -> f.id == item.id and f.type == item.type)
      for _item in _items
        _item._isRemoved = true
        if _item.subject
          pinService.removeBySubject(_item.subject)
          swapLinkedOffers item
      swapEmptyServices()

  pinItem: (pervItem, item) ->
    pinService.pinItem(pervItem, item, loadPinItem)

  restorePins: ->
    pinService.restorePins loadPinItem

  swap: ->
    @items.splice 0, @items.length
    pinService.swapPins()
    historyService.swapHistory()
    itemsService.swap()

  showProfile: ->
    @pushSubjectItem null, { type : "person", id : -1 }

  restoreHistory: ->
    historyService.refreshPoints restorePoint

  removeLink: (s, l)->
    itemsService.removeLinkItem(s, l).then ->
      if l.subject.type == "offer"
        for offerItem in items.filter((f) -> f.subject and f.type == "offer" and f.id == l.subject.id)
          pinService.removeBySubject offerItem.subject
          offerItem._isRemoved = true
      swapEmptyServices()


  regenMailbox : itemsService.regenMailbox
  convertSubject : itemsService.convertSubject
  confirmSubject : itemsService.confirmSubject
