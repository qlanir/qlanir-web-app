"use strict"

app.factory "pinsService", ($resource, $q, webApiConfig, mapper) ->

  resource = $resource(webApiConfig.url + "pins/:id", id: "@id", {update : {method : 'PUT'}})

  getPins : ->
    def = $q.defer()
    resource.query(
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  getPin : (id) ->
    def = $q.defer()
    resource.get(
      id : id,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  createPin : (id, descr) ->
    def = $q.defer()
    resource.save(
      subjectId : id, description : descr
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  updatePin : (id, descr) ->
    def = $q.defer()
    resource.update(
      id : id, description : descr,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  removePin : (id) ->
    def = $q.defer()
    resource.delete(
      id : id,
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),
      ((response) ->
        def.reject(response)
      )
    )

    def.promise

