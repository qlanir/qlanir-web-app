"use strict"


app.factory "cachePins", (pinsService, $q, cacheUtils) ->

  _pins = []

  getCachedPinsBySubject = (subj) ->
    _pins.filter (f)  -> (f.subject.id == subj.id and f.subject.type == subj.type)

  getCachedPinsLinkedToSubjLinks = (subj) ->
    linked = cacheUtils.getLinkedSubjects subj
    _pins.filter (f) ->
      linked.filter((l) -> l.id == f.subject.id and l.type == f.subject.type).length > 0

  refreshPins = (pins) ->
    $q.all(pins.map (m) -> pinsService.getPin(m.id)).then (res) ->
      for s, i in pins
        angular.copy res[i], s
    , (err) ->
      console.log ">>>cache.coffee:110", err

  refreshSubjectPins = (subj) ->
    #if some pin in cache is linked to updated subject
    pinsLinkedDirectlyToSubj = getCachedPinsBySubject subj
    #if some pin in cache is related to any subject's links (updated or removed), it could be updated
    pinsLinkedToSubjLinks = getCachedPinsLinkedToSubjLinks subj

    #concatinate + find unique + refresh
    pinsToRefresh = cacheUtils.getUnique pinsLinkedDirectlyToSubj.concat(pinsLinkedToSubjLinks), (f1, f2) -> f1.id == f2.id

    refreshPins pinsToRefresh

  addPin : (pin) ->
    _pins.push pin

  removePin : (id) ->
    found = _pins.filter((f) -> f.id == id)[0]
    if found
      _pins.splice _pins.indexOf(found), 1

  removePinsBySubject: (subj) ->
    for pin in getCachedPinsBySubject subj
      @removePin pin.id

  refreshSubjectPins : refreshSubjectPins

  swap: ->
    _pins = []

  _pins : _pins