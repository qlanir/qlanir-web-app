"use strict"


app.factory "cache", ($q, cacheRecords, cacheSearchResults, cachePins) ->

  # Subjects

  pushSubject: (subj, isUpdate) ->
    console.log ">>>cache.coffee:9", isUpdate
    cacheRecords.push subj, isUpdate
    if isUpdate
      console.log ">>>cache.coffee:12"
      cacheSearchResults.refreshSearchResults()

  addSubject: (subj) ->
    cacheRecords.push subj
    cacheSearchResults.refreshSearchResults()

  removeSubject: (subj) ->
    cachePins.removePinsBySubject subj
    cacheSearchResults.refreshSearchResults()
    cacheRecords.remove(subj)

  updateSubject: (subj) ->
    $q.all [
      cacheSearchResults.refreshSearchResults()
      cachePins.refreshSubjectPins subj
      cacheRecords.update subj
    ]

  findSubject: cacheRecords.find

  removeItemsNotInList : (itemsList) ->
    cacheRecords.removeRecordsNotInList itemsList.filter (f) -> f.type != "search-results"
    cacheSearchResults.removeItemsNotInList itemsList.filter (f) -> f.type == "search-results"

  _subjs : cacheRecords._cached_records

  #Search results

  addSearchResult: cacheSearchResults.addSearchResult

  findSearchResults: cacheSearchResults.findSearchResults

  removeSearchResult: cacheSearchResults.removeSearchResult

  _searchResults : cacheSearchResults._searchResults

  #Pins

  addPin : cachePins.addPin

  removePin : cachePins.removePin

  _pins : cachePins._pins

  swap: ->
    cacheRecords.swap()
    cacheSearchResults.swap()
    cachePins.swap()
