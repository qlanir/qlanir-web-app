
app.factory "cacheUtils", ->

  isLinksContainSubj = (subj, links) ->
    if links
      links.filter((f) -> f.subject.id == subj.id and f.subject.type == subj.type).length > 0
    else
      false

  mapOfferToService = (subj) ->
    if subj.type == "offer"
      id : subj.tags[0].id, type  : "service"

  mapOffersToServices = (offers) ->
    offers ?= []
    offers.map (m) -> subject : id : m.tag.id, type : "service"

  getLinkedSubjectsFromLinks = (links) ->
    links ?= []
    links.map((f) -> id : f.subject.id, type : f.subject.type)


  isSubjContainsLinkedSubject: (subj, linkedSubj) ->
    isLinksContainSubj(linkedSubj, subj.people) or
    isLinksContainSubj(linkedSubj, subj.organizations) or
    isLinksContainSubj(linkedSubj, subj.contacts) or
    isLinksContainSubj(linkedSubj, subj.offers)

  getLinkedSubjects: (subj) ->

    res = []
    res.push getLinkedSubjectsFromLinks(subj.people)...
    res.push getLinkedSubjectsFromLinks(subj.organizations)...
    res.push getLinkedSubjectsFromLinks(subj.contacts)...
    res.push getLinkedSubjectsFromLinks(subj.offers)...
    res.push getLinkedSubjectsFromLinks(mapOffersToServices(subj.offers))...

    serviceFromOffer = mapOfferToService(subj)
    if serviceFromOffer
      res.push serviceFromOffer

    res

  getUnique: (items, filter) ->
    res = []
    for item in items
      if res.filter((f) -> filter(f, item)).length == 0
        res.push item
    res
