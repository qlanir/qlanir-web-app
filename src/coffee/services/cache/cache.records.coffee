"use strict"


app.factory "cacheRecords", (subjectsService, $q) ->

  _cached_records = []

  findInCache = (rec) ->
    _cached_records.filter((f) -> f.id == rec.id and f.type == rec.type)[0]

  getUnique = (items) ->
    res = []
    for item in items
      if res.filter((f) -> f.id == item.id and f.type == item.type).length == 0
        res.push item
    res

  concatRecordLinks = (record) ->
    links = []
    if record.people
      links.push record.people...
    if record.organizations
      links.push record.organizations...
    if record.offers
      links.push record.offers...
    links

  getLinked = (record) ->
    results = []
    linked = concatRecordLinks(record)
    for link in linked
      results.push id : link.subject.id, type : link.subject.type
      if link.subject.type == "offer"
        #create fake subject which represents service
        results.push id : link.tag.id, type : "service"
    if record.type == "offer"
      results.push id : record.tags[0].id, type : "service"
    results

  refreshRecords = (records) ->
    _getSubject = (subj) ->
      subjectsService.getSubject(subj.id, subj.type).catch (err) ->
        if err.status == 404
          $q.when()
    m = records.map _getSubject
    $q.all(m).then (res) ->
      for s, i in records
        #if not found on server, remove it from cache
        if res[i]
          angular.copy res[i], s
        else
          if s.type == "service"
            s.offers = []
          _cached_records.splice(_cached_records.indexOf(s), 1)

  update = (updatedRecord) ->
    cached = findInCache updatedRecord
    console.log ">>>cache.records.coffee:58", cached, updatedRecord
    if cached and cached != updatedRecord
      angular.copy updatedRecord, cached
    list = []
    #find in cache record, to which updatedRecord is linked
    linkedRecords = getLinked updatedRecord
    for linkedRecord in linkedRecords
      cached = findInCache linkedRecord
      if cached
        list.push cached
    #find in cache records linked to _subject
    #needed when link between records was removed
    for cached in _cached_records
      linked = getLinked cached
      for link in linked
        if link.id == updatedRecord.id && link.type == updatedRecord.type
          list.push cached
    list = getUnique list
    refreshRecords list

  update : update

  remove: (removedRecord) ->
    cached = findInCache removedRecord
    if cached
      _cached_records.splice _cached_records.indexOf(cached), 1
    list = []
    for cached in _cached_records
      linked = getLinked(cached)
      for link in linked
        if (link.id == removedRecord.id && link.type == removedRecord.type) || link.type == "offer" || link.type == "service"
          list.push cached
    list = getUnique list
    refreshRecords(list)

  removeRecordsNotInList: (records) ->
    recordsNotInList = _cached_records.filter (f) -> records.filter((s) -> s.id == f.id and s.type == f.type).length == 0
    for i in recordsNotInList
      _cached_records.splice _cached_records.indexOf(i), 1

  push: (record, isUpdate) ->
    found = findInCache record
    if !found
      _cached_records.push record
    #in case new link was added
    if isUpdate
      update record

  find: findInCache

  swap: ->
    _cached_records = []

  _cached_records: _cached_records


