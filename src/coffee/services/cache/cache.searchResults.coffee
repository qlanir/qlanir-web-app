"use strict"


app.factory "cacheSearchResults", (searchService, $q) ->


  _searchResults = []

  refreshSearchResults =  ->
    $q.all(_searchResults.map (m) -> searchService.search(m.filter)).then (res) ->
      for s, i in _searchResults
        angular.copy res[i], s.searchResult

  addSearchResult: (id, filter, searchResult) ->
    _searchResults.push id : id, filter : filter, searchResult : searchResult

  removeSearchResult: (id) ->
    found = _searchResults.filter((f) -> f.id == id)
    if found
      _searchResults.splice _searchResults.indexOf(found), 1

  removeItemsNotInList : (itemsList) ->
    searchResultItems = itemsList.filter (f) -> f.type == "search-results"
    searchResultsNotInList = _searchResults.filter (f) -> searchResultItems.filter((s) -> s.id == f.id).length == 0
    for i in searchResultsNotInList
      _searchResults.splice _searchResults.indexOf(i), 1

  findSearchResults: (id) ->
    _searchResults.filter((f) -> f.id == id)[0]?.searchResult

  refreshSearchResults : refreshSearchResults

  swap: ->
    console.log ">>>cache.searchResults.coffee:36"
    _searchResults = []

  _searchResults : _searchResults