"use strict"

app.factory "storage", (DSCacheFactory) ->

  cache = DSCacheFactory("qlanir-cache")

  put : (key, val) -> cache.put key, val

  get : (key) -> cache.get key

  push : (key, val) ->
    arr = @get key
    if !arr
      arr = []
    arr.push val
    @put key, arr

  rewrite : (key, index, val) ->
    arr = @get key
    if !arr
      arr = []
    arr.splice index, 1, val
    @put key, arr

  rm: (key) ->
    cache.remove key

