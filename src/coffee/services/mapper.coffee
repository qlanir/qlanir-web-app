"use strict"

app.factory "mapper", ->

  #clone angular resource object as pure json
  clone$ : (obj) ->
    res = angular.copy obj
    for own p of res
      if p.indexOf("$") == 0
        delete res[p]
    res
