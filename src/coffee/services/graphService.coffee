"use strict"

app.factory "graphService", ($http, $q, webApiConfig) ->

  paths : (subjectId) ->
    def = $q.defer()

    $http.get(webApiConfig.url + "graph/paths/" + subjectId).success((res) ->
      def.resolve res
    ).error def.reject

    def.promise
