"use strict"

app.factory "historyService", (storage)  ->

  mapItem2Hist = (item) ->
    id : item.id, type : item.type, name : item.name, subjectType : item.subject.type

  stack : []

  storePoint : (items, onRestore) ->
    items = items.map (m) -> mapItem2Hist m
    item = items[items.length - 1]
    ims = items.map (m) -> m

    lastPoint = if @stack.length > 0 then @stack[@stack.length-1] else null
    penultItemLP = if lastPoint && lastPoint.items.length > 1 then lastPoint.items[lastPoint.items.length-2] else null
    penultItem  = if items.length > 1 then items[items.length - 2] else null

    if lastPoint && lastPoint.items.length == items.length
      if item.type == lastPoint.item.type && (item.type == "search-results" || item.id == lastPoint.item.id)
        @_rewriteLastPoint item, ims, onRestore
      else if penultItem && penultItemLP && (penultItemLP.type == penultItem.type &&  penultItem.subjectType == penultItemLP.subjectType) && ((penultItem.type == "search-results" && penultItem.name == penultItemLP.name) || penultItem.id == penultItemLP.id)
        @_rewriteLastPoint item, ims, onRestore
      else
        @_storePoint item, ims, onRestore
    else
      @_storePoint item, ims, onRestore

  _storePoint : (item, items, onRestore) ->
    @stack.push item : item, items : items, onRestore : onRestore
    storage.push "historyPoints", item : item, items : items

  _rewriteLastPoint : (item, items, onRestore) ->
    if @stack.length == 0
      @_storePoint item, items, onRestore
    else
      @stack.splice @stack.length-1, 1, {item : item, items : items, onRestore : onRestore}
      storage.rewrite "historyPoints", @stack.length-1, item : item, items : items

  refreshPoints: (onRestore) ->
    points = storage.get "historyPoints"
    if points
      for pt in points
        item = pt.item
        items = pt.items
        @stack.push item : item, items : items, onRestore : onRestore
      onRestore items

  restorePoint: (point) ->
    point.onRestore point.items

  swapHistory: ->
    @stack.splice 0, @stack.length
    storage.rm "historyPoints"


