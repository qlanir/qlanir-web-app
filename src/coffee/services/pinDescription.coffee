"use strict"

app.factory "pinDescription", ->

  getItemLink = (pervItem, item) ->
    for lk in pervItem.subject.people
      if lk.subject.id == item.id then return lk
    for lk in pervItem.subject.organizations
      if lk.subject.id == item.id then return lk

  compose: (pervItem, item) ->

    #see alg here : https://docs.google.com/drawings/d/1v98-mFJ5_KAmjZCiI-1H0vqn3uaMqm15kIxeIKQ7VP8/edit

    ###
    if !pervItem
      throw "pervItem should be defined"
    ###
    descr = null

    if !item
      throw "item should be defined"

    if item.type == "offer"

      descr = item.name
      if item.subject.label
        descr  += ": " + item.subject.label

      if  pervItem and (pervItem.type == "person" || pervItem.type == "organization")
        return ref : pervItem.subject.id, descr : descr
      else
        if item.subject.organizations.length == 1
          proxyLink =  item.subject.people.filter( (f) -> f.tag.id == -43)[0]
          return ref : proxyLink.subject.id, descr : descr
        else
          executorLink =  item.subject.people.filter( (f) -> f.tag.id == -42)[0]
          return ref : executorLink.subject.id, descr : descr

    else

      pervItemType = pervItem.type if pervItem

      switch pervItemType

        when "offer"
          descr = pervItem.name
          if pervItem.subject.label
            descr  += ": " + pervItem.subject.label
          return ref : item.subject.id, descr : descr

        when "search-results"
          return ref : item.subject.id, descr : pervItem.name

        else
          if pervItem
            link = getItemLink pervItem, item
            descr = pervItem.name + " " + link.hostTag.label
          return ref : item.subject.id, descr : descr

    throw "argument is wrong !"