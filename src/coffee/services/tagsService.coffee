"use strict"

app.factory "tagsService", ($resource, $q, webApiConfig) ->

  resource = $resource(webApiConfig.url + "tags/:ident/:oper", ident: "@ident", oper: "@oper")
  linkedResource = $resource(webApiConfig.url + "subjects/:subjectId/tags/:label", label: "@label", subjectId: "@subjectId")

  createTag : (label) ->
    def = $q.defer()
    resource.save(
      { ident : label }
      ((res) ->
        console.log "res", res
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  removeTag : (id) ->
    def = $q.defer()
    resource.delete(
      { ident : id }
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  getTagLinksCount : (id) ->
    def = $q.defer()
    resource.get(
      { ident : id, oper : "linksCount" }
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  updateLinkedTag : (subjectId, label) ->
    def = $q.defer()
    linkedResource.save(
      { subjectId : subjectId, label : label }
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise

  removeLinkedTag : (subjectId, label) ->
    def = $q.defer()
    linkedResource.delete(
      { subjectId : subjectId, label : label }
      ((res) ->
        def.resolve(res)
      ),
      ((response) ->
        def.reject(response)
      )
    )
    def.promise
