"use strict"


app.factory "itemsService", ($q, searchService, subjectsService, itemFactory, cache) ->

  createGUID = ->
    s4 = ->
      Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
    s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4()

  removeSubjectLink = (subj, link) ->
    if link.subject.type == "person"
      subj.people.splice subj.people.indexOf(link), 1
    else if link.subject.type == "organization"
      subj.organizations.splice subj.organizations.indexOf(link), 1
    else if link.subject.type == "offer"
      subj.offers.splice subj.offers.indexOf(link), 1
    else if link.subject.type == "contact"
      subj.contacts.splice subj.contacts.indexOf(link), 1

  getSearchItem: (filter, id) ->
    defered = $q.defer()
    if id
      cachedSearchItem = cache.findSearchResults id
    if cachedSearchItem
      item = itemFactory(id, "search-results", cachedSearchItem.term, cachedSearchItem, cachedSearchItem.subjectType)
      defered.resolve item
    else
      searchService.search(filter).then (res) ->
        id ?= createGUID()
        item = itemFactory(id, "search-results", res.term, res, res.type)
        cache.addSearchResult id, filter, res
        defered.resolve(item)
      , (err) ->
        defered.reject(err)
    defered.promise

  removeLinkItem: (subj, link) ->
    if link.subject.type == "offer"
      promise = @removeItem link.subject
    else
      promise = subjectsService.removeLink(subj.id, subj.type, link.id, link.subject.type)
    promise.then (res) ->
      #removeSubjectLink subj, link
      cache.updateSubject res


  popItem: (item) ->
    cache.removeSubject id : item.id, type : item.type

  removeItem: (item) ->
    if item.type != "search-results"
      subjectsService.removeSubject(item.id, item.type).then ->
        cache.removeSubject(id : item.id, type : item.type)
    else
      cache.removeSearchResult item.id
      $q.when()

  getSubjectItem: (subj) ->
    subjId = subj.id
    subjType = subj.type
    deferred = $q.defer()
    cachedSubj = cache.findSubject subj
    if cachedSubj
      item = itemFactory(cachedSubj.id, cachedSubj.type, cachedSubj.label, cachedSubj)
      deferred.resolve(item)
    else
      subjectsService.getSubject(subjId, subjType).then (res) ->
        item = itemFactory(res.id, res.type, res.label, res)
        cache.pushSubject res
        deferred.resolve(item)
      , (err) ->
        deferred.reject err
    deferred.promise

  saveSubject: (subjData, type) ->
    subjId = subjData.id
    subjectsService.saveSubject(subjId, type, subjData).then (res) ->
      if subjId or subjId == 0
        cache.updateSubject res
      else
        cache.pushSubject res, true

  regenMailbox: (item) ->
    subjectsService.regenMailbox().then (data)->
      item.subject.label = data.subject.label

  convertSubject: (item) ->
    subject =
      label: item.subject.label
      id: item.id
      type: item.type
      tags: item.subject.tags.filter((f) -> f.id != -601)
      contacts: item.subject.contacts
    subjectsService.saveSubject("convert", subject.type, subject).then (res) ->
      item.id = res.id;
      item.subject.id = res.id
      item.type = res.type
      item.subject.type = res.type
      cache.updateSubject res

  confirmSubject: (item) ->
    subject =
      label: item.subject.label
      id: item.id
      type: item.type
      tags: item.subject.tags.filter((f) -> f.id != -601)
      contacts: item.subject.contacts
    subjectsService.saveSubject(subject.id, subject.type, subject).then (res) ->
      cache.updateSubject res


  restoreHistoryItems: (historyItems) ->
    #find in cache or load from server history item
    $q.all(historyItems.map((m) => @_restoreHistoryItem(m))).then (items) ->
      #clean up items, which is not in restored items list
      cache.removeItemsNotInList items
      items

  _restoreHistoryItem: (histItem) ->
    if histItem.type == "search-results"
      @getSearchItem {term : histItem.name, type : histItem.subjectType, includeLinkedTags: true}, histItem.id
    else
      @getSubjectItem(id : histItem.id, type : histItem.subjectType).then null, (err) ->
        if err.status == 404
          item = itemFactory(null, null, null, {id : null, type : null})
          item._isRemoved = true
          $q.when item

  swap: -> cache.swap()

