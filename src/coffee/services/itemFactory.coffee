"use strict"

app.value "itemFactory",  (id, type, name, subject, subjectType) ->
  id : id
  type : type
  name : if type == "offer" then subject.tags[0].label else name
  subjectType : subjectType
  subject : subject
  userLinkCaption : ->
    switch @subject.userLinkType
      when "primary"
        return "знакомый"
      when "secondary"
        return "через " + @subject.userLinkProxy
      else
        return "нет связей"
