"use strict"

app.factory "searchService", ($resource, $q, webApiConfig, mapper) ->

  resource = $resource(webApiConfig.url + "search")
  namesResource = $resource(webApiConfig.url + "search/names")

  search : (filter) ->
    def = $q.defer()

    resource.get(
      filter
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )

    def.promise

  searchNames : (filter) ->
    def = $q.defer()

    namesResource.query(
      filter
      ((res) ->
        def.resolve(mapper.clone$ res)
      ),

      ((response) ->
        def.reject(response)
      )
    )

    def.promise
