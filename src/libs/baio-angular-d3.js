"use strict";
var d3app;

d3app = angular.module('baio.d3', []);

"use strict";
d3app.directive('d3Graph', function(d3Service) {
  return {
    restrict: 'EA',
    scope: {
      data: "=d3GraphData",
      load: "=d3GraphLoad"
    },
    link: function(scope, ele) {
      return d3Service.d3().then(function(d3) {
        var force, link, links, node, nodes, render, svg, _getLinkIndex, _getNodeIndex;
        svg = d3.select(ele[0]).append('svg').attr({
          width: "100%",
          height: "500%"
        });
        scope.$watch("data", (function() {
          return render(scope.data);
        }));
        links = [];
        nodes = [];
        link = null;
        node = null;
        force = d3.layout.force().charge(-500).linkDistance(30).linkStrength(0.1).size([500, 500]).nodes(nodes).links(links).on("tick", function() {
          link.attr("x1", function(d) {
            return d.source.x;
          }).attr("y1", function(d) {
            return d.source.y;
          }).attr("x2", function(d) {
            return d.target.x;
          }).attr("y2", function(d) {
            return d.target.y;
          });
          return node.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
          });
        });
        _getLinkIndex = function(d, index) {
          var idx;
          idx = force.links().indexOf(force.links().filter(function(f) {
            return f.attrs.key === d.attrs.key;
          })[0]);
          if (idx === -1) {
            idx = index;
          }
          return idx;
        };
        _getNodeIndex = function(d, index) {
          var idx;
          idx = force.nodes().indexOf(force.nodes().filter(function(f) {
            return f.attrs.id === d.attrs.id;
          })[0]);
          if (idx === -1) {
            idx = index;
          }
          return idx;
        };
        return render = function(data) {
          var existentNode, keys, lk, _i, _len, _ref, _ref1, _ref2;
          if (!data) {
            return;
          }
          _ref = data.links;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            lk = _ref[_i];
            keys = [lk.source.attrs.id, lk.target.attrs.id].sort(d3.ascending);
            lk.attrs.key = keys[0] + "_" + keys[1];
            existentNode = force.nodes().filter(function(f) {
              return f.attrs.id === lk.target.attrs.id;
            })[0];
            if (existentNode) {
              lk.target = existentNode;
            }
            existentNode = force.nodes().filter(function(f) {
              return f.attrs.id === lk.source.attrs.id;
            })[0];
            if (existentNode) {
              lk.source = existentNode;
            }
          }
          (_ref1 = force.links()).push.apply(_ref1, data.links);
          (_ref2 = force.nodes()).push.apply(_ref2, data.nodes);
          link = svg.selectAll(".link").data(force.links(), _getLinkIndex).enter().append("line").attr("class", "link");
          node = svg.selectAll(".node").data(force.nodes(), _getNodeIndex).enter().append("g");
          node.append("circle").attr("class", function(n) {
            return "node node-" + n.attrs.type;
          }).attr("r", 10);
          node.call(force.drag);
          node.on("dblclick", function(n) {
            return scope.load(n.attrs);
          });
          node.append("text").attr("class", "text").style("text-anchor", "middle").text(function(d) {
            return d.attrs.label;
          });
          link = svg.selectAll(".link");
          node = svg.selectAll("g");
          node.sort(function() {
            return -1;
          });
          link.sort(function() {
            return -1;
          });
          return force.start();
        };
      });
    }
  };
});

"use strict";
d3app.factory('d3Service', function($document, $q, $rootScope, D3JS_SCRIPT_PATH) {
  var d, onScriptLoad, s, scriptTag;
  d = $q.defer();
  onScriptLoad = function() {
    return $rootScope.$apply(function() {
      return d.resolve(window.d3);
    });
  };
  scriptTag = $document[0].createElement('script');
  scriptTag.type = 'text/javascript';
  scriptTag.async = true;
  scriptTag.src = D3JS_SCRIPT_PATH;
  scriptTag.onreadystatechange = function() {
    if (this.readyState === 'complete') {
      return onScriptLoad();
    }
  };
  scriptTag.onload = onScriptLoad;
  s = $document[0].getElementsByTagName('body')[0];
  s.appendChild(scriptTag);
  return {
    d3: function() {
      return d.promise;
    }
  };
});
