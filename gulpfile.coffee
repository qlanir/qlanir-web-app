"use strict"

gulp = require "gulp"
template = require "gulp-template"
concat = require "gulp-concat"
jade = require "gulp-jade"
lr = require "tiny-lr"
nodemon = require "gulp-nodemon"
plumber = require "gulp-plumber"
coffee = require "gulp-coffee"
pkg = require "./package.json"
config = require "yaml-config"

$ENV = process.env.NODE_ENV || "dev"
opts = config.readConfig('./app.yml', $ENV)
$VER = pkg.version + " " + (new Date()).toString()

lrServer = lr()

gulp.task "coffee", ->
  gulp.src(["./src/coffee/**/*.coffee"])
  .pipe(plumber())
  .pipe(coffee(bare: true))
  .pipe(concat("app.js"))
  .pipe(gulp.dest("./public/js"))

gulp.task "watch-coffee", ["coffee"], ->
  gulp.watch "./src/coffee/**/*.coffee", ["coffee"]

gulp.task "jade", ->
  locals =
    ENV : $ENV
  gulp.src("./src/jade/*.jade")
  .pipe(plumber())
  .pipe(jade(locals : locals))
  .pipe(gulp.dest('./public/html'))

gulp.task "watcher", ->
  lrServer.listen 35729, (err) ->
    if err
      console.log err

gulp.task "watch-jade", ["jade"], ->
  gulp.watch "./src/jade/**/*.jade", ["jade"]

gulp.task "watch-assets", ->
  lrServer.listen 35729, (err) ->
    if err
      console.log err
  gulp.watch ["./public/js/*.js", "./public/css/*.css", "./public/html/*.html"], (file) ->
    lrServer.changed body : files: [file.path]

gulp.task "nodemon", ->
  nodemon(script : "server.coffee")

gulp.task "create-app-config", ->

  clientOpts =
    webApiUrl : opts.client.webApiUrl
    authApiUrl : opts.client.authApiUrl
    version : $VER
    env : $ENV

  gulp.src("./src/app-config.template.coffee")
  .pipe(template(clientOpts))
  .pipe(concat("app-config.coffee"))
  .pipe(gulp.dest("./src/coffee/configs"))

gulp.task "build", ["create-app-config", "jade", "coffee"], ->
  console.log "env - ", $ENV
  gulp.src "server.coffee"
  .pipe(coffee(debug : true))
  .pipe(gulp.dest('.'))

gulp.task "dev-server", ["create-app-config", "create-app-config", "watch-jade", "watch-coffee", "watch-assets", "nodemon"]

gulp.task "default", ["dev-server"]


