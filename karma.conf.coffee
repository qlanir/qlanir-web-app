"use strict"

module.exports = (config) ->

  config.set

    frameworks: ['jasmine']

    files: [
      "public/bower_components/angular/angular.js",
      "public/bower_components/angular-resource/angular-resource.js",
      "public/bower_components/angular-route/angular-route.js",
      "public/bower_components/angular-sanitize/angular-sanitize.js",
      "public/bower_components/angular-animate/angular-animate.js",
      "public/bower_components/AngularJS-Toaster/toaster.js"
      "public/bower_components/angular-bootstrap/ui-bootstrap.js",
      "public/bower_components/angular-xeditable/dist/js/xeditable.js",
      "public/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
      "public/bower_components/ng-tags-input/ng-tags-input.js"
      "public/bower_components/underscore/underscore.js",
      "public/bower_components/underscore.string/lib/underscore.string.js",
      "public/bower_components/angular-cache/dist/angular-cache.js",
      "public/libs/underscore.inflections.js",

      "public/bower_components/angular-mocks/angular-mocks.js",
      "src/coffee/**/*.coffee",
      "test/unit/**/*Spec.coffee",
      "test/unit/**/*spec.coffee"
    ]

    reporters: ['progress']
    browsers: ['PhantomJS']
    logLevel: config.LOG_DEBUG
    autoWatch: false

    singleRun: false
    colors: true

    preprocessors:
      "src/coffee/**/*.coffee": ["coffee"]
      "test/unit/**/*.coffee": ["coffee"]

    coffeePreprocessor:
      options:
        sourceMap: true
        bare: true
        debug: true
      transformPath: (path) ->
        path.replace( /.js$/, '.coffee' )